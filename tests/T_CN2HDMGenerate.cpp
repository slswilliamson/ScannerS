#include "ScannerS/Constants.hpp"
#include "ScannerS/Models/CN2HDM.hpp"
#include "catch.hpp"
#include <iostream>

TEST_CASE("CN2HDM Generate", "[unit][CN2HDM]") {
  ScannerS::Models::CN2HDM::AngleInput in;
  in.a1 = 0.3224008126;
  in.a2 = -0.9247934565;
  in.a3 = 0.3224008126;
  in.a4 = -1.060084863;
  in.a5 = 0.5279472638;
  in.a6 = -1.306922569;
  in.tbeta = 10;
  in.mHa = 125.09;
  in.mHb = 250;
  in.mHD = 1000;
  in.mHp = 700;
  in.vs = 254.8189541;
  in.re_m12sq = 6000;
  in.type = ScannerS::Models::CN2HDM::Yuk::typeII;
  in.v = ScannerS::Constants::vEW;

  auto p = ScannerS::Models::CN2HDM::ParameterPoint(in);

  std::cout << "\nin.mha = " << in.mHa << " in.mhb = " << in.mHb << "\n"
            << std::endl;
  std::cout << "p.mh1 = " << p.mHi[0] << " p.mh2 = " << p.mHi[1]
            << " p.mh3 = " << p.mHi[2] << " p.mh4 = " << p.mHi[3] << "\n"
            << std::endl;

  CHECK(p.mHi[0] > 0);
  CHECK(p.mHi[1] > 0);
  CHECK(p.mHi[2] > 0);
  CHECK(p.mHi[3] > 0);

  CHECK(p.mHi[2] == Approx(323.438));
  CHECK(p.mHi[3] == Approx(920.167));

  using std::pow;
  using std::sqrt;

  CHECK(p.mHD == in.mHD);
  CHECK(p.mHp == in.mHp);
  CHECK(p.tbeta == in.tbeta);
  CHECK(p.m12sq.real() == in.re_m12sq);

  std::cout << "in mHp = " << in.mHp << " p.mHp = " << p.mHp << "\n"
            << std::endl;
  std::cout << "in mHD = " << in.mHD << " p.mHD = " << p.mHD << "\n"
            << std::endl;

  CHECK(p.type == in.type);
  CHECK(p.vs == in.vs);

  CHECK(p.R(0, 0) >= 0);
  CHECK(p.R(3, 3) >= 0);
  CHECK(p.R(2, 2) +
            (p.R(0, 2) * p.R(0, 3) * p.R(2, 3)) / (1 - pow(p.R(0, 3), 2)) >=
        0);

  CHECK(p.L[0] == Approx(1269.885979));
  CHECK(p.L[1] == Approx(1.680442201));
  CHECK(p.L[2] == Approx(20.16889794));
  CHECK(p.L[3] == Approx(-14.62315704));
  CHECK(p.L[4] == Approx(0.4571773832));
  CHECK(p.L[5] == Approx(3.584740018));
  CHECK(p.L[6] == Approx(4.489649914));
  CHECK(p.L[7] == Approx(26.51268396));
  CHECK(p.L[8] == Approx(-0.2608792623));
  CHECK(p.m11sq == Approx(-931661.6540085420));
  CHECK(p.m22sq == Approx(-47400.0220239100));
  CHECK(p.musq == Approx(926991.2499412320));
  CHECK(p.m12sq.imag() == Approx(10758.4998144313));
}
