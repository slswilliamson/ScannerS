#include "ScannerS/Utilities.hpp"
#include "catch.hpp"

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <algorithm>
#include <array>
#include <cstddef>
#include <iostream>
#include <string>
#include <vector>

namespace {
const std::vector<std::array<double, 6>> inputangles{
    {1.30002, -0.287791, -0.583279, 0.00399173, -0.885762, -1.40789},
    {1.16119, -0.523478, -0.105102, -0.363178, -1.23982, -1.04976},
    {-0.020246, -1.55178, 0.392403, -0.692572, -1.04743, -1.32069},
    {-0.386071, 1.18117, -0.664503, -0.151923, -0.731857, 0.897996},
    {-1.2396, 0.555497, 0.246676, 0.0781196, -1.00009, 0.820738},
    {-1.56205, 1.11188, 1.42335, 1.17003, 1.52526, 1.42423},
    {1.43865, -1.53498, -0.736346, 0.845335, 0.0802703, 1.41799},
    {1.48291, -0.297209, 0.684696, -0.320633, 0.565135, 0.528692},
    {-0.0332645, 0.0364523, 0.956528, 0.0633053, 1.09097, 1.3519},
    {-0.394649, -1.10172, -0.0228755, -1.25957, -0.442973, -0.856711},
    {-0.807885, 1.16085, -0.866687, -0.997056, -1.0513, 0.903744},
    {-0.0871554, -0.631694, 1.19097, 1.29344, 0.76904, -0.478707},
    {-1.47132, 0.693314, 1.19114, 0.810968, -0.40749, 1.06423},
    {0.863471, -1.51537, -1.49771, -0.132503, 0.977401, 1.46713},
    {0.121818, 0.401085, 1.10145, 0.741392, 0.258489, 1.35605},
    {-0.072708, 0.265377, 0.221769, -0.484363, 1.51159, -1.00518},
    {0.870425, -0.137308, -0.867738, 1.16377, -0.726278, 0.612352},
    {-1.44579, 1.3116, -0.563731, 0.284319, 0.749003, -0.376135},
    {-0.739047, 0.937916, -1.2462, -1.46633, -0.136228, -0.635991},
    {-0.378909, -1.01418, 1.04533, 0.249472, 1.12994, -1.06839},
    {1.28578, 0.214308, -0.917352, 0.112425, -0.385647, -1.21384},
    {0.377029, 0.754811, -0.706511, 0.504268, 0.765772, 1.26868},
    {-0.409879, -0.145786, -0.406537, -1.07059, -0.42185, 1.23573},
    {-1.07095, 1.21049, 0.272375, -1.08475, -0.868018, 0.188414},
    {1.27857, 0.18282, 1.47268, -0.765029, 0.00614558, 0.0131004},
    {-0.488371, -0.663044, -0.231611, -1.22566, 1.26026, -1.18983},
    {0.753065, -0.899943, -0.207453, 0.609776, 0.345463, 0.124633},
    {0.18893, 0.173837, 1.09043, 0.907423, 0.790749, -0.831289},
    {-0.714308, -0.826363, 0.358623, -0.593948, -0.573763, 0.423454},
    {-0.549348, 0.350154, -0.222023, -1.31883, -1.35006, 0.515636},
    {0.167378, 1.2086, -0.776118, -0.528419, -0.268831, -0.119108},
    {0.891033, -0.166812, 0.0134382, 1.01859, 1.35578, -1.3977},
    {0.121683, -1.04907, 0.190953, 0.594753, -0.449999, 0.69632},
    {0.837164, 1.36862, -1.23699, 0.333176, 1.12899, 0.894451},
    {-0.924755, 0.306754, 0.502395, 0.910292, -1.4557, 0.350873},
    {-0.300842, 0.558728, -0.348136, 1.31013, -1.27513, 1.08717},
    {-0.68195, -0.319115, -1.52207, -0.724113, -1.41951, 1.26585},
    {1.17751, -0.304659, -1.2841, 0.563556, 1.28992, -0.735631},
    {1.50607, 1.33627, 0.185016, -0.367597, 0.734076, 1.57005},
    {-0.239481, 0.989106, 0.721109, 0.0615354, 1.07282, -0.54351},
    {1.36362, 1.27116, -0.554292, 1.56747, -0.478815, -1.07879},
    {-0.982025, 1.49038, 0.19672, -0.0543696, -0.934903, 0.596416},
    {0.0991697, 0.40113, 1.1401, -0.572919, -1.13198, 1.05677},
    {1.22599, 0.985052, -1.46797, -0.690308, -0.507445, -0.711035},
    {0.454767, 1.42642, 0.855496, 0.345518, 0.922749, -1.13845},
    {-0.314356, -0.336806, 0.549843, 0.576719, -1.23294, 0.0235948},
    {0.0178759, 1.07665, -1.39638, 1.42401, 0.515769, -1.13699},
    {-0.524826, -0.68958, 1.15222, -1.51383, -1.49116, -0.42945},
    {1.18426, -1.30521, 0.308277, 0.527337, 1.3361, -1.32537}};

const std::vector<std::array<double, 4>> permutations{
    {0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2},
    {0, 3, 2, 1}, {1, 0, 2, 3}, {1, 0, 3, 2}, {1, 2, 0, 3}, {1, 2, 3, 0},
    {1, 3, 0, 2}, {1, 3, 2, 0}, {2, 0, 1, 3}, {2, 0, 3, 1}, {2, 1, 0, 3},
    {2, 1, 3, 0}, {2, 3, 0, 1}, {2, 3, 1, 0}, {3, 0, 1, 2}, {3, 0, 2, 1},
    {3, 1, 0, 2}, {3, 1, 2, 0}, {3, 2, 0, 1}, {3, 2, 1, 0}};

} // namespace

TEST_CASE("Index Sort 6 angles", "[unit][utils]") {
  for (const auto &x : inputangles) {
    auto indices = ScannerS::Utilities::IndexSort(x);
    std::array<double, 6> test{x};
    std::sort(test.begin(), test.end());
    for (size_t i = 0; i != x.size(); ++i) {
      REQUIRE(x[indices[i]] == Approx(test[i]));
    }
  }
}

TEST_CASE("Ordering 4D Mixing Matrix", "[unit][utils]") {
  for (const auto &ia : inputangles) {
    using std::cos;
    using std::sin;
    const double c1 = cos(ia[0]);
    const double s1 = sin(ia[0]);
    const double c2 = cos(ia[1]);
    const double s2 = sin(ia[1]);
    const double c3 = cos(ia[2]);
    const double s3 = sin(ia[2]);
    const double c4 = cos(ia[3]);
    const double s4 = sin(ia[3]);
    const double c5 = cos(ia[4]);
    const double s5 = sin(ia[4]);
    const double c6 = cos(ia[5]);
    const double s6 = sin(ia[5]);
    Eigen::Matrix4d MixMat4d;
    MixMat4d << c1 * c2 * c6, c2 * c6 * s1, c6 * s2, -s6, // row1
        -(s1 * (c3 * c4 + s3 * s4 * s5)) +
            c1 * (-(s2 * (c4 * s3 - c3 * s4 * s5)) - c2 * c5 * s4 * s6),
        c1 * (c3 * c4 + s3 * s4 * s5) +
            s1 * (-(s2 * (c4 * s3 - c3 * s4 * s5)) - c2 * c5 * s4 * s6),
        c2 * (c4 * s3 - c3 * s4 * s5) - c5 * s2 * s4 * s6,
        -(c5 * c6 * s4), // row2
        c5 * s1 * s3 + c1 * (-(c3 * c5 * s2) - c2 * s5 * s6),
        -(c1 * c5 * s3) + s1 * (-(c3 * c5 * s2) - c2 * s5 * s6),
        c2 * c3 * c5 - s2 * s5 * s6, -(c6 * s5), // row3
        -(s1 * (c3 * s4 - c4 * s3 * s5)) +
            c1 * (-(s2 * (s3 * s4 + c3 * c4 * s5)) + c2 * c4 * c5 * s6),
        c1 * (c3 * s4 - c4 * s3 * s5) +
            s1 * (-(s2 * (s3 * s4 + c3 * c4 * s5)) + c2 * c4 * c5 * s6),
        c2 * (s3 * s4 + c3 * c4 * s5) + c4 * c5 * s2 * s6, c4 * c5 * c6; // row4
    Eigen::Matrix4d test{MixMat4d};

    for (const auto &perm : permutations) {
      test =
          Eigen::PermutationMatrix<4>{
              Eigen::Map<Eigen::Vector4i>{
                  ScannerS::Utilities::IndexSort(perm).data()}}
              .transpose() *
          test;

      ScannerS::Utilities::MixMatNormalForm4d(test);
      auto angles = ScannerS::Utilities::MixMatAngles4d(test);
      const double c1ang = cos(angles[0]);
      const double s1ang = sin(angles[0]);
      const double c2ang = cos(angles[1]);
      const double s2ang = sin(angles[1]);
      const double c3ang = cos(angles[2]);
      const double s3ang = sin(angles[2]);
      const double c4ang = cos(angles[3]);
      const double s4ang = sin(angles[3]);
      const double c5ang = cos(angles[4]);
      const double s5ang = sin(angles[4]);
      const double c6ang = cos(angles[5]);
      const double s6ang = sin(angles[5]);
      Eigen::Matrix4d MixMat4dang;
      MixMat4dang << c1ang * c2ang * c6ang, c2ang * c6ang * s1ang,
          c6ang * s2ang, -s6ang, // row1
          -(s1ang * (c3ang * c4ang + s3ang * s4ang * s5ang)) +
              c1ang * (-(s2ang * (c4ang * s3ang - c3ang * s4ang * s5ang)) -
                       c2ang * c5ang * s4ang * s6ang),
          c1ang * (c3ang * c4ang + s3ang * s4ang * s5ang) +
              s1ang * (-(s2ang * (c4ang * s3ang - c3ang * s4ang * s5ang)) -
                       c2ang * c5ang * s4ang * s6ang),
          c2ang * (c4ang * s3ang - c3ang * s4ang * s5ang) -
              c5ang * s2ang * s4ang * s6ang,
          -(c5ang * c6ang * s4ang), // row2
          c5ang * s1ang * s3ang +
              c1ang * (-(c3ang * c5ang * s2ang) - c2ang * s5ang * s6ang),
          -(c1ang * c5ang * s3ang) +
              s1ang * (-(c3ang * c5ang * s2ang) - c2ang * s5ang * s6ang),
          c2ang * c3ang * c5ang - s2ang * s5ang * s6ang,
          -(c6ang * s5ang), // row3
          -(s1ang * (c3ang * s4ang - c4ang * s3ang * s5ang)) +
              c1ang * (-(s2ang * (s3ang * s4ang + c3ang * c4ang * s5ang)) +
                       c2ang * c4ang * c5ang * s6ang),
          c1ang * (c3ang * s4ang - c4ang * s3ang * s5ang) +
              s1ang * (-(s2ang * (s3ang * s4ang + c3ang * c4ang * s5ang)) +
                       c2ang * c4ang * c5ang * s6ang),
          c2ang * (s3ang * s4ang + c3ang * c4ang * s5ang) +
              c4ang * c5ang * s2ang * s6ang,
          c4ang * c5ang * c6ang; // row4

      INFO("angles1 = " + std::to_string(angles[0]));
      INFO("angles2 = " + std::to_string(angles[1]));
      INFO("angles3 = " + std::to_string(angles[2]));
      INFO("angles4 = " + std::to_string(angles[3]));
      INFO("angles5 = " + std::to_string(angles[4]));
      INFO("angles6 = " + std::to_string(angles[5]));

      INFO("ia1 = " + std::to_string(ia[0]));
      INFO("ia2 = " + std::to_string(ia[1]));
      INFO("ia3 = " + std::to_string(ia[2]));
      INFO("ia4 = " + std::to_string(ia[3]));
      INFO("ia5 = " + std::to_string(ia[4]));
      INFO("ia6 = " + std::to_string(ia[5]));
      INFO("perm = (" + std::to_string(perm[0]) + std::to_string(perm[1]) +
           std::to_string(perm[2]) + std::to_string(perm[3]) +
           std::to_string(perm[4]) + std::to_string(perm[5]) + ")");

      REQUIRE(test.isApprox(Eigen::Matrix4d{MixMat4dang}, 1e-8));
    }
  }
}
