#pragma once

#include <array>
#include <cstddef>
#include <gsl/gsl_integration.h>
#include <memory>

//As in the C2HDM file but adapted to encorporate a 4th Higgs.

//! additional functionality for ScannerS
namespace ScannerS::Tools {

/**
 * @brief Value and individual contributions of the electron EDM.
 * Equation numbers correspond to [1311.4704](https://arxiv.org/abs/1311.4704).
 * All values are in units of \f$e\,\mathrm{cm}\f$.
 */
struct ElectronEDM {
  double contribF;   //!< contribution of fermion loops, Eq. (B.1)
  double contribHp;  //!< contribution of charged Higgs loops, Eq. (B.5)
  double contribW;   //!< contribution of W loops, Eq. (B.7)
  double contribHpW; //!< contribution of H+W-gamma, Eq. (B.9)
  double value;      //!< summed total value for the electron EDM
};

/**
 * @brief Input for the EDM calculation.
 */
struct CN2HEDMInput {
  double mHp;                //!< charged Higgs mass
  std::array<double, 4> mHi; //!< neutral Higgs masses
  /**
   * @brief The neutral Higgs fermion couplings
   *
   * The four indices enumerate the four neutral Higgs bosons (as ordered
   * above), the three fermions [top, bottom, tau] and the two CP parts of the
   * coupling [even, odd].
   */
  std::array<std::array<std::array<double, 2>, 3>, 4> c_Hff;
  /**
   * @brief The neutral Higgs gauge couplings.
   *
   * The index enumerates the four neutral Higgs bosons (as ordered above).
   */
  std::array<double, 4> c_HVV;
  /**
   * @brief The neutral Higgs charged Higgs couplings.
   *
   * The index enumerates the four neutral Higgs bosons (as ordered above).
   */
  std::array<double, 4> c_HHpHm;
};

/**
 * @brief The CN2HEDM class.
 * This class calculates the electron EDM in the complex 2HDM based on the
 * computation of [1311.4704](https://arxiv.org/abs/1311.4704).
 */
class CN2HEDM {
public:
  /**
   * Calculates the electron EDM for the given parameters.
   * @param  in  parameter input
   * @return     electron EDM with individual contributions
   */
  ElectronEDM operator()(const CN2HEDMInput &in) const;

private:
  /**
   * The fermion loop contributions.
   * Calculated using eq (B.1) of 1311.4704.
   * @param  iferm        which fermion to calculate for (up: 0, down: 1, e: 2)
   * @param  c_Hi_ffj_cpk neutral Higgs to fermion couplings
   * @param  mHisq        squared neutral Higgs masses
   * @return              the edm contribution
   */
  double
  edm_F(size_t iferm,
        const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
        const std::array<double, 4> &mHisq) const;

  /**
   * The charged Higgs loop contributions.
   * Calculated using eq (B.5) of 1311.4704.
   * @param  iferm        which fermion to calculate for (up: 0, down: 1, e: 2)
   * @param  c_Hi_ffj_cpk neutral Higgs to fermion couplings
   * @param  c_Hi_HpHm    neutral Higgs to charged Higgs couplings
   * @param  mHisq        squared neutral Higgs masses
   * @param  mHpsq        squared charged Higgs mass
   * @return              the edm contribution
   */
  double edm_Hp(
      size_t iferm,
      const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
      const std::array<double, 4> &c_Hi_HpHm,
      const std::array<double, 4> &mHisq, double mHpsq) const;

  /**
   * The W loop contributions.
   * Calculated using eq (B.7) of 1311.4704.
   * @param  iferm        which fermion to calculate for (up: 0, down: 1, e: 2)
   * @param  c_Hi_ffj_cpk neutral Higgs to fermion couplings
   * @param  c_Hi_VV      neutral Higgs gauge couplings
   * @param  mHisq        squared neutral Higgs masses
   * @return              the edm contribution
   */
  double
  edm_W(size_t iferm,
        const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
        const std::array<double, 4> &c_Hi_VV,
        const std::array<double, 4> &mHisq) const;

  /**
   * The charged Higgs W loop contributions.
   * Calculated using eq (B.9) of 1311.4704.
   * @param  iferm        which fermion to calculate for (up: 0, down: 1, e: 2)
   * @param  c_Hi_ffj_cpk neutral Higgs to fermion couplings
   * @param  c_Hi_VV      neutral Higgs gauge couplings
   * @param  c_Hi_HpHm    neutral Higgs to charged Higgs couplings
   * @param  mHisq        squared neutral Higgs masses
   * @param  mHpsq        squared charged Higgs mass
   * @return              the edm contribution
   */
  double edm_HW(
      size_t iferm,
      const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
      const std::array<double, 4> &c_Hi_VV,
      const std::array<double, 4> &c_Hi_HpHm,
      const std::array<double, 4> &mHisq, double mHpsq) const;

  using GSLIntegrationWorkspace =
      std::unique_ptr<gsl_integration_workspace,
                      decltype(&gsl_integration_workspace_free)>;

  //! Integration workspace
  mutable GSLIntegrationWorkspace integrator = GSLIntegrationWorkspace{
      gsl_integration_workspace_alloc(100), gsl_integration_workspace_free};

  /**
   * The required loop functions.
   * The four functions I_1,2,4,5 from eq. (B.4) and (B.11).
   * Numeric integation with the gsl library is used.
   * @param  m1sq first mass parameter
   * @param  m2sq second mass parameter
   * @param  i    which function to use out of (1,2,4,5)
   * @param mHpsq squared charged Higgs mass (only needed for i=4,5)
   * @return      the function value
   */
  double func_I(double m1sq, double m2sq, int i, double mHpsq = 0) const;
};
} // namespace ScannerS::Tools
