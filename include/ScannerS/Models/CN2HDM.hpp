#pragma once

#include "ScannerS/DataMap.hpp"
#include "ScannerS/Models/TwoHDM.hpp"
#include <Eigen/Core>
#include <array>
#include <complex>
#include <cstddef>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace ScannerS {
namespace Interfaces {
namespace HiggsBoundsSignals {
template <int nHzero, int nHplus> struct HBInput;
template <size_t nHzero, size_t nHplus> class HiggsBoundsSignals;
} // namespace HiggsBoundsSignals
} // namespace Interfaces

namespace Tools {
class SushiTables;
} // namespace Tools

namespace Constraints::STUDetail {
struct STUParameters;
}

namespace Models {
/**
 * @brief The CP-violating 2HDM
 *
 * This implementation follows the conventions used in
 * [1711.09419](http://arxiv.org/abs/arXiv:1711.09419).
 *
 */
class CN2HDM {
public:
  //! short description
  static constexpr auto description = "CP-violating N2HDM";
  //! number of neutral Higgs bosons
  static constexpr int nHzero = 5;
  //! number of charged Higgs bosons
  static constexpr int nHplus = 1;
  //! charged Higgs names, \f$ H^\pm \f$
  static constexpr std::array namesHplus{"Hp"};
  //! neutral Higgs names, \f$ H_1, H_2, H_3, H_4, H_D \f$
  static constexpr std::array namesHzero{"H1", "H2", "H3", "H4", "HD"};

  //! Yukawa types
  using Yuk = TwoHDM::Yuk;

  struct AngleInput {
    double mHa; //!< \f$ m_{H_a} \f$, neutral Higgs mass in GeV
    double mHb; //!< \f$ m_{H_b} \f$, neutral Higgs mass in GeV
    double mHD; //!< \f$ m_{H_D} \f$, dark Higgs mass in GeV
    double mHp; //!< \f$ m_{H^\pm} \f$, charged Higgs mass in GeV
    double a1; //!< \f$ \alpha_1 \f$ mixing angle
    double a2; //!< \f$ \alpha_2 \f$ mixing angle
    double a3; //!< \f$ \alpha_3 \f$ mixing angle
    double a4; //!< \f$ \alpha_4 \f$ mixing angle
    double a5; //!< \f$ \alpha_5 \f$ mixing angle
    double a6; //!< \f$ \alpha_6 \f$ mixing angle
    double tbeta;    //!< \f$\tan\beta\f$, ratio of vacuum expectation values
    double re_m12sq; //!< \f$ \Re(m_{12}^2) \f$, in \f$\mathrm{GeV}^2\f$
    double vs; //!< \f$ v_s \f$ singlet vev in GeV
    Yuk type; //!< the Yukawa type
    double v; //!< EW vev in GeV
  };

  //! A CN2HDM parameter point
  struct ParameterPoint {
    //! mass-ordered neutral Higgs masses \f$m_{H_{1,2,3,4}}\f$ in GeV
    const std::array<double, nHzero - 1> mHi;
    const double mHD;
    //! charged Higgs mass \f$m_{H^\pm}\f$ in GeV
    const double mHp;
    //! \f$\tan\beta\f$, ratio of vacuum expectation values
    const double tbeta;
    //! mass-ordered neutral mixing matrix \f$R\f$
    const Eigen::Matrix<double, nHzero - 1, nHzero - 1> R;
    //! the corresponding mixing angles \f$\alpha_{1,2,3, 4, 5, 6}\f$
    const std::array<double, 6> alpha;
    //! quartic potential parameters
    //! \f$\lambda_{1,2,3,4},\Re(\lambda_5),\Im(\lambda_5), \lambda_6, \lambda_7, \lambda_8\f$
    const std::array<double, 9> L;
    //! soft \f$Z_2\f$ breaking parameter \f$m_{12}^2\f$ in \f$\mathrm{GeV}^2\f$
    const std::complex<double> m12sq;
    //! doublet mass term \f$m_{11}^2\f$ in \f$\mathrm{GeV}^2\f$
    const double m11sq;
    //! doublet mass term \f$m_{22}^2\f$ in \f$\mathrm{GeV}^2\f$
    const double m22sq;
    //! \f$ \mu_s^2 \f$, in \f$\mathrm{GeV}^2\f$
    const double musq;
    //! Singlet vev in GeV
    const double vs;
    //! Yukawa type
    const Yuk type;
    //! EW vev in GeV
    const double v;
    //! place for additional data
    DataMap data;

    //! Construct a ParameterPoint from AngleInput
    explicit ParameterPoint(const AngleInput &in);

    //! output names for the parameters

    static constexpr std::array parameterNames{
        "mH1",      "mH2",   "mH3",     "mH4",   "mHD",   "mHp", "tbeta",
        "a1",       "a2",    "a3",      "a4",    "a5",    "a6",  "re_m12sq",
        "im_m12sq", "vs",    "yuktype", "v",     "R11",   "R12", "R13",
        "R14",      "R21",   "R22",     "R23",   "R24",   "R31", "R32",
        "R33",      "R34",   "R41",     "R42",   "R43",   "R44", "L1",
        "L2",       "L3",    "L4",      "re_L5", "im_L5", "L6",  "L7",
        "L8",       "m11sq", "m22sq",   "musq"};

    //! serialize the parameter and data values for output
    std::string ToString() const;
  };

  static inline bool Valid(const ParameterPoint &p) {
    return (p.mHi[0] > 0) && (p.R(0, 0) < 1);
  }

  /**
   * @brief Model implementation for Constraints::BFB
   *
   * Uses the TwoHDM::BFB implementation.
   *
   * @param L the quartic parameters of the scalar potential
   * @return is the scalar potential at `p` bounded from below?
   */
  static bool BFB(const std::array<double, 9> &L);

  /**
   * @brief Model implementation for Constraints::Unitarity
   *
   * Uses the TwoHDM::MaxUnitarityEV implementation.
   *
   * @param L Quartic couplings
   * @return maxEV, absolute value of largest eigenvalue
   */
  static double MaxUnitarityEV(const std::array<double, 9> &L);

  /**
   * @brief Model implementation for Constraints::STU
   * @param p the parameter point
   * @return Constraints::STUDetail::STUParameters input parameters for the STU
   * calculation
   */
  static Constraints::STUDetail::STUParameters
  STUInput(const ParameterPoint &p);

  /**
   * @brief Model implementation for Constraints::STU
   * @param p the parameter point
   * @return is the oblique parameter approximation applicable for `p`
   */
  static bool EWPValid(const ParameterPoint &p);

  /**
   * @brief Runs Hdecay for the given parameter point.
   *
   * Uses the AnyHdecay interface to call c2hdm_hdecay. Results are stored in
   * p.data with the names given in AnyHdecay::Hdecay::c2hdmKeys.
   *
   * @param p the parameter point
   */

  static void RunHdecay(ParameterPoint &p);

  /**
   * @brief Calculates Higgs couplings.
   *
   * Calculates and stores the effective gauge couplings \f$c(H_iVV)\f$
   * (`c_HiVV`) and \f$c(H_iH_jZ)\f$ (`c_HiHjZ`) as well as the CP-even
   * (\f$c^e(H_if\bar{f})\f$, `_e`) and CP-odd (\f$c^o(H_if\bar{f})\f$, `_o`)
   * fermion couplings `c_Hiuu_e/o` `c_Hidd_e/o`, `c_Hill_e/o` for each `Hi` and
   * `Hj` in #namesHzero.
   *
   * @param p the parameter point
   */
  static void CalcCouplings(ParameterPoint &p);

  /**
   * @brief Calculate and store some LHC production cross sections
   *
   * **Requires CalcCouplings to be called beforehand.**
   *
   * Stores the 13TeV LHC \f$gg\to H_i\f$ cross sections as `x_Hi_ggH` and the
   * \f$pp\to b\bar{b}H_i\f$ cross sections as `x_Hi_bbH` for `Hi` in
   * #namesHzero. Uses Tools::SushiTables.
   *
   * @param p the parameter point
   */
  static void CalcCXNs(ParameterPoint &p);

  /**
   * @brief Model implementation for Constraints::Higgs
   *
   * **Requires RunHdecay and CalcCouplings to be called beforehand.**
   *
   * Uses the HiggsBounds tables to obtain and store the 13TeV LHC \f$pp\to
   * \bar{t}bH^+\f$ cross section as `x_tHc` (in pb).
   *
   * @param p the parameter point
   * @param hbhs the HiggsBoundsSignals object to access tabulated values
   * @return Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> input for
   * HiggsBounds
   */
  static Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus>
  HiggsBoundsInput(
      ParameterPoint &p,
      const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
          &hbhs);

  /**
   * @brief Model implementation for Constraints::ElectronEDM
   *
   * Calculates the electric dipole moment of the electron using Tools::C2HEDM.
   * Stores the individual contributions (all in \f$e\,\mathrm{cm}\f$):
   *  - the contribution of fermion loops as `edm_e_f`
   *  - the contribution of charged Higgs loops as `edm_e_Hc`
   *  - the contribution of W-boson loops as `edm_e_W`
   *  - the \f$H^\pm W^\mp\gamma\f$ contribution as `edm_e_HcW`
   *
   * The total electron EDM is stored by the constraint.
   *
   * @param p the parameter point
   * @return double prediction for the electron EDM in \f$e\,\mathrm{cm}\f$
   */
  static double CalcElectronEDM(ParameterPoint &p);

  /**
   * @brief Model implementation for Constraints::DM
   * @param p the parameter point
   * @return std::map<std::string, double> of MicrOMEGAs input parameters
   */
  static std::map<std::string, double> MOInput(const ParameterPoint &p);
  //! Constraints::DM MicrOMEGAs model name
  // Set to default
  static constexpr auto micromegasModelName = "CN2HDM_T2";

private:
  static const Tools::SushiTables cxnH0_;
};
} // namespace Models
} // namespace ScannerS
