#include "ScannerS/Models/N2HDMDarkSD.hpp"
#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <cmath>
#include <map>
#include <sstream>
#include <utility>

namespace {
std::array<double, 8>
CalcLambdas(const ScannerS::Models::N2HDMDarkSD::Input &in) {
  using std::pow;
  const double vsq = pow(in.v, 2);

  const double L1 = pow(in.mHsm, 2) / vsq;
  const double L3 = 2. * (pow(in.mHDp, 2) - in.m22sq) / vsq;
  const double L4 =
      (pow(in.mAD, 2) + pow(in.mHDD, 2) - 2 * pow(in.mHDp, 2)) / vsq;
  const double L5 = (pow(in.mHDD, 2) - pow(in.mAD, 2)) / vsq;
  const double L7 = 2. * (pow(in.mHDS, 2) - in.mssq) / vsq;
  return {L1, in.L2, L3, L4, L5, in.L6, L7, in.L8};
}

double CalcM11sq(double mHsm) { return -pow(mHsm, 2) / 2.; }

} // namespace

namespace ScannerS::Models {
N2HDMDarkSD::ParameterPoint::ParameterPoint(const Input &in)
    : mHsm{in.mHsm}, mHDD{in.mHDD}, mAD{in.mAD}, mHDp{in.mHDp}, mHDS{in.mHDS},
      L{CalcLambdas(in)}, m11sq{CalcM11sq(in.mHsm)}, m22sq{in.m22sq},
      mssq{in.mssq}, v{in.v} {}

std::string N2HDMDarkSD::ParameterPoint::ToString() const {
  auto os = std::ostringstream{};
  auto printer = Utilities::TSVPrinter(os);
  printer << mHsm << mHDD << mAD << mHDp << mHDS;
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq << mssq;
  printer << v;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

Constraints::STUDetail::STUParameters
N2HDMDarkSD::STUInput(const ParameterPoint &p) {
  return N2HDM::STUInput(p.mAD, {p.mHsm, p.mHDD, p.mHDS}, p.mHDp, 0,
                         Eigen::Matrix3d::Identity());
}

bool N2HDMDarkSD::EWPValid(const ParameterPoint &p) {
  return (p.mHDp + p.mHDD > Constants::mW) &&
         (p.mHDp + p.mAD > Constants::mW) && (p.mHDD + p.mAD > Constants::mZ) &&
         (2 * p.mHDp > Constants::mZ);
}

void N2HDMDarkSD::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.n2hdmDarkSingletDoublet(
      DarkAMass{p.mAD}, DarkHcMass{p.mHDp}, DarkHMass{p.mHDD}, HMass{p.mHsm},
      DarkHMass{p.mHDS}, SquaredMassPar{p.m22sq}, SquaredMassPar{p.mssq});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<N2HDMDarkSD::nHzero,
                                        N2HDMDarkSD::nHplus>
N2HDMDarkSD::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &) {

  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  hb.CP_value(0) = 1;
  // masses including dark Higgs bosons
  hb.Mh << p.mHsm, p.mHDD, p.mHDS, p.mAD;

  hb.GammaTotal_hj(0) = p.data["w_Hsm"];

  // HiggsBounds_neutral_input_SMBR
  hb.BR_hjss(0) = p.data["b_Hsm_ss"];
  hb.BR_hjcc(0) = p.data["b_Hsm_cc"];
  hb.BR_hjbb(0) = p.data["b_Hsm_bb"];
  hb.BR_hjtt(0) = p.data["b_Hsm_tt"];
  hb.BR_hjmumu(0) = p.data["b_Hsm_mumu"];
  hb.BR_hjtautau(0) = p.data["b_Hsm_tautau"];
  hb.BR_hjZga(0) = p.data["b_Hsm_Zgam"];
  hb.BR_hjgaga(0) = p.data["b_Hsm_gamgam"];
  hb.BR_hjgg(0) = p.data["b_Hsm_gg"];
  hb.BR_hjWW(0) = p.data["b_Hsm_WW"];
  hb.BR_hjZZ(0) = p.data["b_Hsm_ZZ"];
  hb.BR_hjinvisible(0) = p.data["b_Hsm_HDDHDD"] + p.data["b_Hsm_ADAD"] +
                         p.data["b_Hsm_HDpHDm"] + p.data["b_Hsm_HDSHDS"];

  // HiggsBounds_neutral_input_LEP
  hb.XS_ee_hjZ_ratio(0) = 1;
  hb.XS_ee_bbhj_ratio(0) = 1;
  hb.XS_ee_tautauhj_ratio(0) = 1;
  // HiggsBounds_neutral_input_hadr
  //   Tevatron
  hb.TEV_CS_hj_ratio(0) = 1;
  hb.TEV_CS_gg_hj_ratio(0) = 1;
  hb.TEV_CS_bb_hj_ratio(0) = 1;
  hb.TEV_CS_hjW_ratio(0) = 1;
  hb.TEV_CS_hjZ_ratio(0) = 1;
  hb.TEV_CS_vbf_ratio(0) = 1;
  hb.TEV_CS_tthj_ratio(0) = 1;
  hb.TEV_CS_thj_tchan_ratio(0) = 1;
  hb.TEV_CS_thj_schan_ratio(0) = 1;
  //   LHC7
  hb.LHC7_CS_hj_ratio(0) = 1;
  hb.LHC7_CS_gg_hj_ratio(0) = 1;
  hb.LHC7_CS_bb_hj_ratio(0) = 1;
  hb.LHC7_CS_hjW_ratio(0) = 1;
  hb.LHC7_CS_hjZ_ratio(0) = 1;
  hb.LHC7_CS_vbf_ratio(0) = 1;
  hb.LHC7_CS_tthj_ratio(0) = 1;
  hb.LHC7_CS_thj_tchan_ratio(0) = 1;
  hb.LHC7_CS_thj_schan_ratio(0) = 1;
  //   LHC8
  hb.LHC8_CS_hj_ratio(0) = 1;
  hb.LHC8_CS_gg_hj_ratio(0) = 1;
  hb.LHC8_CS_bb_hj_ratio(0) = 1;
  hb.LHC8_CS_hjW_ratio(0) = 1;
  hb.LHC8_CS_hjZ_ratio(0) = 1;
  hb.LHC8_CS_vbf_ratio(0) = 1;
  hb.LHC8_CS_tthj_ratio(0) = 1;
  hb.LHC8_CS_thj_tchan_ratio(0) = 1;
  hb.LHC8_CS_thj_schan_ratio(0) = 1;
  //   LHC13
  hb.LHC13_CS_hj_ratio(0) = 1;
  hb.LHC13_CS_gg_hj_ratio(0) = 1;
  hb.LHC13_CS_bb_hj_ratio(0) = 1;
  hb.LHC13_CS_hjW_ratio(0) = 1;
  hb.LHC13_CS_hjZ_ratio(0) = 1;
  hb.LHC13_CS_gg_hjZ_ratio(0) = 1;
  hb.LHC13_CS_qq_hjZ_ratio(0) = 1;
  hb.LHC13_CS_vbf_ratio(0) = 1;
  hb.LHC13_CS_tthj_ratio(0) = 1;
  hb.LHC13_CS_thj_tchan_ratio(0) = 1;
  hb.LHC13_CS_thj_schan_ratio(0) = 1;
  hb.LHC13_CS_tWhj_ratio(0) = 1;
  return hb;
}

std::vector<double> N2HDMDarkSD::ParamsEVADE(const ParameterPoint &p) {
  return {0,      p.v,    0,      p.L[0], p.L[1],  p.L[2],  p.L[3], p.L[4],
          p.L[5], p.L[6], p.L[7], 0,      p.m11sq, p.m22sq, p.mssq};
}

std::map<std::string, double> N2HDMDarkSD::MOInput(const ParameterPoint &p) {
  return {{"ms2", p.mssq},
          {"M11s", p.m11sq},
          {"M22s", p.m22sq},
          {"Lam1", p.L[0]},
          {"Lam2", p.L[1]},
          {"Lam3", p.L[2]},
          {"Lam4", p.L[3]},
          {"Lam5", p.L[4]},
          {"Lam6", p.L[5]},
          {"Lam7", p.L[6]},
          {"Lam8", p.L[7]},
          {"MZ", Constants::mZ},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"alfSMZ", Constants::alphaSAtMz},
          {"aEWinv", 1 / Constants::alphaAtMz},
          {"Mu2", Constants::mC},
          {"Md3", Constants::mB},
          {"Mu3", Constants::mT},
          {"Me2", Constants::mMu},
          {"Me3", Constants::mTau}};
}

} // namespace ScannerS::Models
