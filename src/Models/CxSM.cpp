#include "ScannerS/Models/CxSM.hpp"
#include "ScannerS/Constants.hpp"
#include <algorithm>
#include <cmath>

namespace ScannerS::Models {

bool CxSM::BFB(const std::array<double, 3> &L) {
  return (L[0] > 0) && (L[1] > 0) && (L[2] > 0 || L[2] * L[2] < L[0] * L[1]);
}

double CxSM::MaxUnitarityEV(const std::array<double, 3> &L) {
  using ScannerS::Constants::pi;
  using std::abs, std::pow, std::sqrt;
  return std::max(
      {abs(L[0]) / 2., abs(L[1]) / 2., abs(L[2]) / 2.,
       abs((2 * L[1] - sqrt(8 * pow(L[2], 2) + pow(2 * L[1] - 3 * L[0], 2)) +
            3 * L[0]) /
           4.),
       abs((2 * L[1] + sqrt(8 * pow(L[2], 2) + pow(2 * L[1] - 3 * L[0], 2)) +
            3 * L[0]) /
           4.)});
}

} // namespace ScannerS::Models
