#include "ScannerS/Models/CPVDM.hpp"

#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/ScalarWidths.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <numeric>
#include <sstream>

namespace ScannerS::Models {

namespace {
std::array<double, 3> ThreeMasses(double mHa, double mHb,
                                  const std::array<double, 3> &alphain) {
  const auto R = Utilities::MixMat3d(alphain[0], alphain[1], alphain[2]);
  const double mHzsq =
      -(mHa * mHa * R(0, 0) * R(0, 1) + mHb * mHb * R(1, 0) * R(1, 1)) /
      (R(2, 0) * R(2, 1));
  return {mHa, mHb, mHzsq > 0 ? std::sqrt(mHzsq) : -1};
}

Eigen::Matrix3d MixMat(double mHa, double mHb,
                       const std::array<double, 3> &alphain) {
  return Utilities::OrderedMixMat3d(alphain[0], alphain[1], alphain[2],
                                    ThreeMasses(mHa, mHb, alphain));
}

std::array<double, 8> Lambdas(double mHsm, const std::array<double, 3> &mHi,
                              double mHp, const Eigen::Matrix3d &R, double v,
                              double m22sq, double mssq, double L2, double L6,
                              double L8) {
  using std::pow;
  const double mH1sq = pow(mHi[0], 2);
  const double mH2sq = pow(mHi[1], 2);
  const double mH3sq = pow(mHi[2], 2);
  const double mHpsq = pow(mHp, 2);
  const double vsq = pow(v, 2);

  const double L4 =
      (mH3sq * R(0, 2) - 2 * mHpsq * R(0, 2) -
       2 * mH1sq * R(0, 1) * R(1, 1) * R(1, 2) -
       mH1sq * R(0, 2) * pow(R(1, 2), 2) - 2 * mH3sq * R(1, 0) * R(2, 1) +
       4 * mHpsq * R(1, 0) * R(2, 1) +
       mH2sq * (R(0, 2) + 2 * R(0, 1) * R(1, 1) * R(1, 2) +
                R(0, 2) * pow(R(1, 2), 2) - 2 * R(1, 0) * R(2, 1)) +
       2 * (mH1sq - mH3sq) * R(0, 1) * R(2, 1) * R(2, 2) +
       (mH1sq - mH3sq) * R(0, 2) * pow(R(2, 2), 2)) /
      ((R(0, 2) - 2 * R(1, 0) * R(2, 1)) * vsq);

  const double L5 = (R(0, 2) * (mH3sq + mH2sq * (-1 + pow(R(1, 2), 2)) -
                                mH3sq * pow(R(2, 2), 2) +
                                mH1sq * (-pow(R(1, 2), 2) + pow(R(2, 2), 2)))) /
                    ((R(0, 2) - 2 * R(1, 0) * R(2, 1)) * vsq);
  const double L7 =
      (-2 * (mH2sq * R(1, 2) * (2 * R(0, 1) * R(1, 1) + R(0, 2) * R(1, 2)) +
             mH3sq * R(0, 0) * R(2, 0) * R(2, 2) -
             mH3sq * R(0, 1) * R(2, 1) * R(2, 2) -
             mH1sq * (2 * R(0, 1) * R(1, 1) * R(1, 2) - 2 * R(1, 0) * R(2, 1) -
                      2 * R(0, 1) * R(2, 1) * R(2, 2) +
                      R(0, 2) * (1 + pow(R(1, 2), 2) - pow(R(2, 2), 2))) +
             R(0, 2) * mssq - 2 * R(1, 0) * R(2, 1) * mssq)) /
      ((R(0, 2) - 2 * R(1, 0) * R(2, 1)) * vsq);

  return {
      pow(mHsm, 2) / vsq, L2, (2 * (mHpsq - m22sq)) / vsq, L4, L5, L6, L7, L8};
}

std::complex<double> Trilinear(const std::array<double, 3> &mHi,
                               const Eigen::Matrix3d &R, double v) {
  using std::pow;
  const double mH1sq = pow(mHi[0], 2);
  const double mH2sq = pow(mHi[1], 2);
  const double mH3sq = pow(mHi[2], 2);
  const double Tr = ((-mH1sq + mH2sq) * R(0, 0) * pow(R(1, 0), 2) +
                     (mH1sq - mH3sq) * R(0, 0) * pow(R(2, 0), 2)) /
                    ((R(0, 2) - 2 * R(1, 0) * R(2, 1)) * v);
  const double Ti = ((-mH1sq + mH2sq) * R(0, 1) * pow(R(1, 1), 2) +
                     (mH1sq - mH3sq) * R(0, 1) * pow(R(2, 1), 2)) /
                    ((R(0, 2) - 2 * R(1, 0) * R(2, 1)) * v);
  return {Tr, Ti};
}

} // namespace

CPVDM::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHsm{in.mHsm}, mHi{Utilities::Sorted(
                         ThreeMasses(in.mHa, in.mHb, {in.a1, in.a2, in.a3}))},
      mHp{in.mHp}, R{MixMat(in.mHa, in.mHb, {in.a1, in.a2, in.a3})},
      L{Lambdas(mHsm, mHi, mHp, R, in.v, in.m22sq, in.mssq, in.L2, in.L6,
                in.L8)},
      A{Trilinear(mHi, R, in.v)}, m11sq{-std::pow(in.mHsm, 2) / 2.},
      m22sq{in.m22sq}, mssq{in.mssq}, v{in.v}, alpha{Utilities::MixMatAngles3d(
                                                   R)} {}

std::string CPVDM::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  printer << mHsm;
  for (double m : mHi)
    printer << m;
  printer << mHp;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  for (double x : L) {
    printer << x;
  }
  printer << A.real() << A.imag();
  printer << m11sq << m22sq << mssq;
  printer << v;
  for (double a : alpha)
    printer << a;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

Constraints::STUDetail::STUParameters CPVDM::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(2, nHzero + 1),
      Eigen::DiagonalMatrix<double, 2>(1, 1),
      {p.mHsm, p.mHi[0], p.mHi[1], p.mHi[2]},
      {p.mHp}};
  res.mV << std::complex<double>(0, 1), 1, 0, 0, 0, 0, 0,
      std::complex<double>(p.R(0, 0), p.R(0, 1)),
      std::complex<double>(p.R(1, 0), p.R(1, 1)),
      std::complex<double>(p.R(2, 0), p.R(2, 1));
  return res;
}

bool CPVDM::EWPValid(const ParameterPoint &p) {
  return (2 * p.mHi[0] > Constants::mZ) && (2 * p.mHp > Constants::mZ) &&
         (p.mHi[0] + p.mHp > Constants::mW);
}

std::map<std::string, double> CPVDM::MOInput(const ParameterPoint &p) {
  double a2 = std::asin(p.R(0, 2));
  double a1 = std::atan(p.R(0, 1) / p.R(0, 0));
  double a3 = std::atan(p.R(1, 2) / p.R(2, 2));
  return {{"MZ", Constants::mZ},
          {"MW", Constants::mW},
          {"aEWM1", Constants::alphaAtMz},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"mHsm", p.mHsm},
          {"mHc", p.mHp},
          {"alph1", a1},
          {"alph2", a2},
          {"alph3", a3},
          {"mH1", p.mHi[0]},
          {"mH2", p.mHi[1]},
          {"m22sq", p.m22sq},
          {"mssq", p.mssq},
          {"L2", p.L[1]},
          {"L6", p.L[5]},
          {"L8", p.L[7]},
          {"MC", Constants::mC},
          {"MB", Constants::mB},
          {"MT", Constants::mT},
          {"MM", Constants::mMu},
          {"MTA", Constants::mTau}};
}

std::vector<double> CPVDM::ParamsEVADE(const ParameterPoint &p) {
  return {p.v,    p.L[0],    p.L[1],     p.L[2],     p.L[3],  p.L[4],  p.L[5],
          p.L[6], p.L.at(7), p.A.real(), p.A.imag(), p.m11sq, p.m22sq, p.mssq};
}

void CPVDM::CalcCouplings(ParameterPoint &p) {
  auto tripleHCoups = [&p](int i, int j) {
    return p.R(i, 1) * ((p.L[2] + p.L[3] - p.L[4]) * p.v * p.R(j, 1) -
                        p.A.imag() * p.R(j, 2)) +
           p.R(i, 0) * ((p.L[2] + p.L[3] + p.L[4]) * p.v * p.R(j, 0) +
                        p.A.real() * p.R(j, 2)) +
           p.R(i, 2) * (p.A.real() * p.R(j, 0) - p.A.imag() * p.R(j, 1) +
                        p.L[6] * p.v * p.R(j, 2));
  };
  p.data.Store("c_HsmH1H1", tripleHCoups(0, 0));
  p.data.Store("c_HsmH2H2", tripleHCoups(1, 1));
  p.data.Store("c_HsmH3H3", tripleHCoups(2, 2));
  p.data.Store("c_HsmH1H2", tripleHCoups(0, 1));
  p.data.Store("c_HsmH1H3", tripleHCoups(0, 2));
  p.data.Store("c_HsmH2H3", tripleHCoups(1, 2));

  p.data.Store("c_HsmHpHm", p.v * p.L[2]);
}

Interfaces::HiggsBoundsSignals::HBInputEffC<CPVDM::nHzero, CPVDM::nHplus>
CPVDM::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  auto in = Interfaces::HiggsBoundsSignals::HBInputEffC<nHzero, nHplus>{};
  // neutral Higgs
  in.Mh << p.mHsm, p.mHi[0], p.mHi[1], p.mHi[2];
  in.SetSMlikeScaled({1, 0, 0, 0});
  in.ghjgaga(0) =
      sqrt(ScalarWidths::EffHsmGamGam(p.mHp, p.data["c_HsmHpHm"]));

  auto smwidths = ScalarWidths::ScaledSMWidths(hbhs.GetSMBRs(p.mHsm), 1);
  smwidths.w_h_gamgam *= std::pow(in.ghjgaga(0), 2);

  using ScalarWidths::TripleHiggsDecEqual, ScalarWidths::TripleHiggsDec;
  auto invwidths = std::array{
      TripleHiggsDecEqual(p.mHsm, p.mHi[0], p.data["c_HsmH1H1"]),
      TripleHiggsDecEqual(p.mHsm, p.mHi[1], p.data["c_HsmH2H2"]),
      TripleHiggsDecEqual(p.mHsm, p.mHi[2], p.data["c_HsmH3H3"]),
      TripleHiggsDec(p.mHsm, p.mHi[0], p.mHi[1], p.data["c_HsmH1H2"]),
      TripleHiggsDec(p.mHsm, p.mHi[0], p.mHi[2], p.data["c_HsmH1H3"]),
      TripleHiggsDec(p.mHsm, p.mHi[1], p.mHi[2], p.data["c_HsmH2H3"])};
  p.data.Merge(SMLikeBRs(
      smwidths, std::accumulate(invwidths.begin(), invwidths.end(), 0.),
      "Hsm"));

  p.data.Store("b_Hsm_H1H1", invwidths[0] / p.data["w_Hsm"]);
  p.data.Store("b_Hsm_H2H2", invwidths[1] / p.data["w_Hsm"]);
  p.data.Store("b_Hsm_H3H3", invwidths[2] / p.data["w_Hsm"]);
  p.data.Store("b_Hsm_H1H2", invwidths[3] / p.data["w_Hsm"]);
  p.data.Store("b_Hsm_H1H3", invwidths[4] / p.data["w_Hsm"]);
  p.data.Store("b_Hsm_H2H3", invwidths[5] / p.data["w_Hsm"]);
  in.BR_hjinvisible(0) = p.data["b_Hsm_H1H1"] + p.data["b_Hsm_H2H2"] +
                         p.data["b_Hsm_H3H3"] + p.data["b_Hsm_H1H2"] +
                         p.data["b_Hsm_H1H3"] + p.data["b_Hsm_H2H3"];

  in.Mhplus(0) = p.mHp;
  return in;
}

} // namespace ScannerS::Models
