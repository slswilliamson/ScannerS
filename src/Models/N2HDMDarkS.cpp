#include "ScannerS/Models/N2HDMDarkS.hpp"

#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <cmath>
#include <cstddef>
#include <map>
#include <sstream>
#include <stdexcept>
#include <utility>

// -------------------------------- Generation --------------------------------
namespace {
double CalcSortedAlpha(double mHa, double mHb, double alphaIn) {
  using ScannerS::Constants::pi;
  if (mHa >= mHb)
    return alphaIn;
  else {
    if (sin(alphaIn) > 0)
      return alphaIn - pi / 2;
    else
      return alphaIn + pi / 2;
  }
}

// get the ordering matching Eq. (16) in 1805.00966
auto DarkSingletMixmat(double alpha) {
  Eigen::Vector3i trans;
  trans << 1, 0, 2;
  auto result = ScannerS::Utilities::OrderedMixMat3d(-alpha, 0, 0, {0, 1, 2});
  result *= Eigen::PermutationMatrix<3>{trans}.transpose();
  return result;
}

std::array<double, 8> CalcLambdas(const std::array<double, 2> &mHi, double mA,
                                  double mHp, double tbeta,
                                  const Eigen::Matrix3d &R, double v,
                                  double m12sq, double L6, double L7,
                                  double L8) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double L1 =
      1 / pow(v * cb, 2) *
      (pow(mHi[0] * R(0, 0), 2) + pow(mHi[1] * R(1, 0), 2) - m12sq * tbeta);
  const double L2 =
      1 / pow(v * sb, 2) *
      (pow(mHi[0] * R(0, 1), 2) + pow(mHi[1] * R(1, 1), 2) - m12sq / tbeta);
  const double L3 = 1. / v / v / sb / cb *
                        (pow(mHi[0], 2) * R(0, 0) * R(0, 1) +
                         pow(mHi[1], 2) * R(1, 0) * R(1, 1) - m12sq) +
                    2 * pow(mHp / v, 2);
  const double L4 =
      1 / pow(v, 2) * (mA * mA - 2 * mHp * mHp) + m12sq / v / v / sb / cb;
  const double L5 = -pow(mA / v, 2) + m12sq / v / v / sb / cb;
  return {L1, L2, L3, L4, L5, L6, L7, L8};
}

double CalcM11sq(double tbeta, double v, double m12sq,
                 const std::array<double, 8> &L) {
  const double v1 = cos(atan(tbeta)) * v;
  const double v2 = sin(atan(tbeta)) * v;
  return tbeta * m12sq -
         1 / 2. * (v1 * v1 * L[0] + v2 * v2 * (L[2] + L[3] + L[4]));
}

double CalcM22sq(double tbeta, double v, double m12sq,
                 const std::array<double, 8> &L) {
  const double v1 = cos(atan(tbeta)) * v;
  const double v2 = sin(atan(tbeta)) * v;
  return m12sq / tbeta -
         1 / 2. * (v2 * v2 * L[1] + v1 * v1 * (L[2] + L[3] + L[4]));
}

double CalcMssq(double mHD, double tbeta, double v,
                const std::array<double, 8> &L) {
  const double v1 = cos(atan(tbeta)) * v;
  const double v2 = sin(atan(tbeta)) * v;
  return -1 / 2. * (v1 * v1 * L[6] + v2 * v2 * L[7] - 2 * mHD * mHD);
}

} // namespace

namespace ScannerS::Models {

N2HDMDarkS::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(std::array<double, 2>({in.mHa, in.mHb}))},
      mA{in.mA}, mHp{in.mHp}, mHD{in.mHD}, tbeta{in.tbeta},
      alpha{CalcSortedAlpha(in.mHb, in.mHa, in.alpha)},
      R{DarkSingletMixmat(alpha)}, type{in.type}, v{in.v}, m12sq{in.m12sq},
      L{CalcLambdas(mHi, mA, mHp, tbeta, R, v, m12sq, in.L6, in.L7, in.L8)},
      m11sq{CalcM11sq(tbeta, v, m12sq, L)},
      m22sq{CalcM22sq(tbeta, v, m12sq, L)}, mssq{CalcMssq(mHD, tbeta, v, L)} {}

// -------------------------------- Pheno --------------------------------
void N2HDMDarkS::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.n2hdmDarkSinglet(
      static_cast<Yukawa>(p.type), TanBeta(p.tbeta), SquaredMassPar(p.m12sq),
      AMass(p.mA), HcMass(p.mHp), DarkHMass(p.mHD), HMass(p.mHi[0]),
      HMass(p.mHi[1]), MixAngle(p.alpha), Lambda(p.L[6]), Lambda(p.L[7]));
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<N2HDMDarkS::nHzero, N2HDMDarkS::nHplus>
N2HDMDarkS::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {

  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t I = 1; I != nHzeroVisible; ++I) {
    hb.CP_value(I) = 1;
    hb.Mh(I) = p.mHi[I - 1];
  }
  hb.Mh(0) = p.mA;
  hb.CP_value(0) = -1;

  for (size_t i = 0; i != nHzeroVisible; ++i) {
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
  }
  for (size_t I = 1; I != nHzeroVisible; ++I) {
    hb.BR_hjWW(I) = p.data["b_"s + namesHzero[I] + "_WW"];
    hb.BR_hjZZ(I) = p.data["b_"s + namesHzero[I] + "_ZZ"];
    hb.BR_hjinvisible(I) = p.data["b_"s + namesHzero[I] + "_HDHD"];
  }
  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(2, 1, 1) = p.data["b_H2_H1H1"];
  for (size_t I = 1; I != nHzeroVisible; ++I) {
    hb.BR_hkhjhi(I, 0, 0) = p.data["b_"s + namesHzero[I] + "_AA"];
    hb.BR_hjhiZ(I, 0) = p.data["b_"s + namesHzero[I] + "_ZA"];
    hb.BR_hjhiZ(0, I) = p.data["b_A_Z"s + namesHzero[I]];
  }
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    hb.BR_hjHpiW(i, 0) = p.data["b_"s + namesHzero[i] + "_WpHm"];
  }

  for (size_t I = 1; I != nHzeroVisible; ++I) {
    const double cVV = p.data["c_"s + namesHzero[I] + "VV"];
    const double cu = p.data["c_"s + namesHzero[I] + "uu_e"];
    const double cd = p.data["c_"s + namesHzero[I] + "dd_e"];
    const double cl = p.data["c_"s + namesHzero[I] + "ll_e"];
    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(I) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(I) = pow(cd, 2);
    hb.XS_ee_tautauhj_ratio(I) = pow(cl, 2);
    hb.XS_ee_hjhi_ratio(I, 0) = pow(p.data["c_"s + namesHzero[I] + "AZ"], 2);
    hb.XS_ee_hjhi_ratio(0, I) = hb.XS_ee_hjhi_ratio(I, 0);
    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg =
        cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::TEV);
    double bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::TEV);
    double gg0 =
        cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(I) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(I) = bb / bb0;
    hb.TEV_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(I) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC8
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(I) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    //   LHC13
    gg = cxnH0_.GGe(p.mHi[I - 1], cu, cd, Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(p.mHi[I - 1], cd, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[I - 1], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[I - 1], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(I) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(I) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(I) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[I - 1], cVV, cu, cd);
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[I - 1], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(I) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(I) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(I) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(I) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(I) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(I) = pow(cu, 2);      //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(I) = pow(cu, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(I) =
        Interfaces::HiggsBoundsSignals::tWHratio(cVV, cu);
  }

  // --- same for A ---
  const double cu = p.data["c_Auu_o"];
  const double cd = p.data["c_Add_o"];
  const double cl = p.data["c_All_o"];
  // HiggsBounds_neutral_input_LEP
  hb.XS_ee_bbhj_ratio(0) = pow(cd, 2);
  hb.XS_ee_tautauhj_ratio(0) = pow(cl, 2);
  // HiggsBounds_neutral_input_hadr
  //   Tevatron
  double gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::TEV);
  double bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::TEV);
  double gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::TEV);
  double bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::TEV);
  hb.TEV_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.TEV_CS_gg_hj_ratio(0) = gg / gg0;
  hb.TEV_CS_bb_hj_ratio(0) = bb / bb0;
  hb.TEV_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.TEV_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.TEV_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC7
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC7);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC7);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC7);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC7);
  hb.LHC7_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC7_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC7_CS_bb_hj_ratio(0) = bb / bb0;
  hb.LHC7_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC7_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC7_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC8
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC8);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC8);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC8);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC8);
  hb.LHC8_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC8_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC8_CS_bb_hj_ratio(0) = bb / bb0;
  hb.LHC8_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC8_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC8_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  //   LHC13
  gg = cxnH0_.GGo(p.mA, cu, cd, Tools::SushiTables::Collider::LHC13);
  bb = cxnH0_.BBo(p.mA, cd, Tools::SushiTables::Collider::LHC13);
  gg0 = cxnH0_.GGe(p.mA, 1, 1, Tools::SushiTables::Collider::LHC13);
  bb0 = cxnH0_.BBe(p.mA, 1, Tools::SushiTables::Collider::LHC13);
  hb.LHC13_CS_hj_ratio(0) = (gg + bb) / (gg0 + bb0);
  hb.LHC13_CS_gg_hj_ratio(0) = gg / gg0;
  hb.LHC13_CS_bb_hj_ratio(0) = bb / bb0;
  const auto x_VH = hbhs.GetVHCxns(p.mA, 0, {0, cu}, {0, cd});
  const auto x_VH_ref = hbhs.GetVHCxns(p.mA, 1, 1, 1);
  hb.LHC13_CS_hjZ_ratio(0) = x_VH.x_hZ / x_VH_ref.x_hZ;
  hb.LHC13_CS_gg_hjZ_ratio(0) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
  hb.LHC13_CS_qq_hjZ_ratio(0) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
  hb.LHC13_CS_hjW_ratio(0) = 0.;
  hb.LHC13_CS_tthj_ratio(0) = pow(cu, 2);      //< neglects ZH>ttH diagrams
  hb.LHC13_CS_thj_tchan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC13_CS_thj_schan_ratio(0) = pow(cu, 2); //< LO correct
  hb.LHC13_CS_tWhj_ratio(0) =
      Interfaces::HiggsBoundsSignals::tWHratio(0., {0, cu});

  // ---- charged Higgs ----
  hb.Mhplus(0) = p.mHp;
  hb.GammaTotal_Hpj(0) = p.data["w_Hp"];
  hb.CS_ee_HpjHmj_ratio(0) = 1;
  hb.BR_tWpb = p.data["b_t_Wb"];
  hb.BR_tHpjb(0) = p.data["b_t_Hpb"];
  hb.BR_Hpjcs(0) = p.data["b_Hp_cs"];
  hb.BR_Hpjcb(0) = p.data["b_Hp_cb"];
  hb.BR_Hpjtaunu(0) = p.data["b_Hp_taunu"];
  hb.BR_Hpjtb(0) = p.data["b_Hp_tb"];
  // BR_HpjWZ(0) == 0
  for (size_t i = 0; i != nHzeroVisible; ++i)
    hb.BR_HpjhiW(0, i) = p.data["b_Hp_W"s + namesHzero[i]];

  // charged Higgs cxn (as of Oct 2018 the only one needed)
  auto coupsHp = TwoHDM::TwoHDMHpCoups(p.tbeta, p.type);
  p.data.Store("x_tHpm", hbhs.GetHpCxn(p.mHp, coupsHp.rhot, coupsHp.rhob,
                                       p.data["b_t_Hpb"]));
  hb.LHC13_CS_Hpjtb(0) = p.data["x_tHpm"];

  // dark Higgs mass
  hb.Mh(nHzeroVisible) = p.mHD;
  return hb;
}

Constraints::STUDetail::STUParameters
N2HDMDarkS::STUInput(const ParameterPoint &p) {
  return N2HDM::STUInput(p.mA, {p.mHi[0], p.mHi[1], p.mHD}, p.mHp, p.tbeta,
                         p.R);
}

bool N2HDMDarkS::EWPValid(const ParameterPoint &p) {
  return (p.mHp + p.mHi[0] > Constants::mW) && (p.mHp + p.mA > Constants::mW) &&
         (p.mHi[0] + p.mA > Constants::mZ) && (2 * p.mHp > Constants::mZ);
}

std::map<std::string, double>
Models::N2HDMDarkS::MOInput(const ParameterPoint &p) {
  if (p.type != Yuk::typeI)
    throw std::runtime_error(
        "DM calculations are only supported in the T1 N2HDMDarkS.");
  return {{"ms2", p.mssq},
          {"M11s", p.m11sq},
          {"M12s", p.m12sq},
          {"M22s", p.m22sq},
          {"Lam1", p.L[0]},
          {"Lam2", p.L[1]},
          {"Lam3", p.L[2]},
          {"Lam4", p.L[3]},
          {"Lam5", p.L[4]},
          {"Lam6", p.L[5]},
          {"Lam7", p.L[6]},
          {"Lam8", p.L[7]},
          {"betaH", atan(p.tbeta)},
          {"MZ", Constants::mZ},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"alfSMz", Constants::alphaSAtMz},
          {"v", Constants::vEW},
          {"Mu2", Constants::mC},
          {"Md3", Constants::mB},
          {"Mu3", Constants::mT},
          {"Me2", Constants::mMu},
          {"Me3", Constants::mTau}};
}

// -------------------------------- Properties --------------------------------

const Tools::SushiTables N2HDMDarkS::cxnH0_;

std::string N2HDMDarkS::ParameterPoint::ToString() const {
  auto os = std::ostringstream{};
  auto printer = Utilities::TSVPrinter(os);
  for (double x : mHi)
    printer << x;
  printer << mA << mHp << mHD;
  printer << tbeta << alpha;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  printer << static_cast<int>(type);
  printer << v << m12sq;
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq << mssq;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void N2HDMDarkS::CalcCouplings(ParameterPoint &p) {
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));
  // SM couplings
  for (size_t i = 0; i != nHzero - 2; ++i) {
    p.data.Store("c_"s + namesHzero[i + 1] + "VV",
                 cb * p.R(i, 0) + sb * p.R(i, 1));
    p.data.Store("c_"s + namesHzero[i + 1] + "AZ",
                 (sb * p.R(i, 0) - cb * p.R(i, 1)));
    p.data.Store("c_"s + namesHzero[i + 1] + "uu_e", p.R(i, 1) / sb);
    if (p.type == Yuk::typeI) {
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 1) / sb);
    }
    if (p.type == Yuk::typeII) {
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 0) / cb);
    }
    if (p.type == Yuk::leptonSpecific) { // Lepton Specific
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 0) / cb);
    }
    if (p.type == Yuk::flipped) { // flipped
      p.data.Store("c_"s + namesHzero[i + 1] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i + 1] + "ll_e", p.R(i, 1) / sb);
    }
  }
  p.data.Store("c_Auu_o", 1 / p.tbeta);
  if (p.type == Yuk::typeI) {
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }
  if (p.type == Yuk::typeII) {
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::leptonSpecific) {
    p.data.Store("c_Add_o", -1 / p.tbeta);
    p.data.Store("c_All_o", p.tbeta);
  }
  if (p.type == Yuk::flipped) {
    p.data.Store("c_Add_o", p.tbeta);
    p.data.Store("c_All_o", -1 / p.tbeta);
  }
}

void N2HDMDarkS::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 1; i != nHzero - 1; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(p.mHi.at(i - 1),
                            p.data["c_"s + namesHzero[i] + "uu_e"],
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BBe(p.mHi.at(i - 1),
                            p.data["c_"s + namesHzero[i] + "dd_e"],
                            Tools::SushiTables::Collider::LHC13));
  }
  p.data.Store("x_A_ggH", cxnH0_.GGo(p.mA, p.data["c_Auu_o"], p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
  p.data.Store("x_A_bbH", cxnH0_.BBo(p.mA, p.data["c_Add_o"],
                                     Tools::SushiTables::Collider::LHC13));
}

std::vector<double> N2HDMDarkS::ParamsEVADE(const ParameterPoint &p) {
  return {atan(p.tbeta), p.v,     0.,      p.L[0],  p.L[1],
          p.L[2],        p.L[3],  p.L[4],  p.L[5],  p.L[6],
          p.L[7],        p.m12sq, p.m11sq, p.m22sq, p.mssq};
}
} // namespace ScannerS::Models
