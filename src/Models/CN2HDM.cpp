#include "ScannerS/Models/CN2HDM.hpp"
#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Interfaces/MicrOMEGAs/MicromegasInterface.hpp"
#include "ScannerS/Tools/C2HEDM.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <map>
#include <optional>
#include <sstream>
#include <string>
#include <utility>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------

namespace {
std::array<double, 4> CN2HDMFourMasses(const std::array<double, 6> &alphain,
                                       double tbeta, double mHa, double mHb) {
  using std::pow;
  auto R = Utilities::MixMat4d(alphain[0], alphain[1], alphain[2], alphain[3],
                               alphain[4], alphain[5]);

  double mHfsq =
      (pow(mHa, 2) * (R(0, 3) * (R(0, 2) * R(2, 0) - R(0, 0) * R(2, 2)) -
                      R(0, 2) * R(0, 3) * R(2, 1) * tbeta +
                      R(0, 1) * R(0, 3) * R(2, 2) * tbeta)) /
          (R(3, 3) * (R(2, 2) * R(3, 0) - R(2, 0) * R(3, 2) +
                      (-R(2, 2) * R(3, 1) + R(2, 1) * R(3, 2)) * tbeta)) +
      (pow(mHb, 2) * (R(1, 3) * (R(1, 2) * R(2, 0) - R(1, 0) * R(2, 2)) -
                      R(1, 2) * R(1, 3) * R(2, 1) * tbeta +
                      R(1, 1) * R(1, 3) * R(2, 2) * tbeta)) /
          (R(3, 3) * (R(2, 2) * R(3, 0) - R(2, 0) * R(3, 2) +
                      (-R(2, 2) * R(3, 1) + R(2, 1) * R(3, 2)) * tbeta));

  double mHf = std::sqrt(mHfsq);

  double mHzsq =
      (pow(mHa, 2) *
       (-R(0, 2) * R(0, 3) * R(3, 0) + R(0, 0) * R(0, 3) * R(3, 2) +
        R(0, 3) * (R(0, 2) * R(3, 1) - R(0, 1) * R(3, 2)) * tbeta)) /
          (R(2, 3) * (R(2, 2) * R(3, 0) - R(2, 0) * R(3, 2) +
                      (-R(2, 2) * R(3, 1) + R(2, 1) * R(3, 2)) * tbeta)) +
      (pow(mHb, 2) *
       (-R(1, 2) * R(1, 3) * R(3, 0) + R(1, 0) * R(1, 3) * R(3, 2) +
        R(1, 3) * (R(1, 2) * R(3, 1) - R(1, 1) * R(3, 2)) * tbeta)) /
          (R(2, 3) * (R(2, 2) * R(3, 0) - R(2, 0) * R(3, 2) +
                      (-R(2, 2) * R(3, 1) + R(2, 1) * R(3, 2)) * tbeta));

  double mHz = std::sqrt(mHzsq);

  return {mHa, mHb, mHz > 0 ? std::sqrt(mHzsq) : -1,
          mHf > 0 ? std::sqrt(mHfsq) : -1};
}

////Quartic couplings/////

std::array<double, 9> Lambdas(double tbeta, double v, double vs,
                              const std::array<double, 4> &mHi, double mHp,
                              const Eigen::Matrix4d &R, double re_m12sq) {
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));

///////// Matrix elements /////////

  const double X11 = pow(mHi[0] * R(0, 0), 2) + pow(mHi[1] * R(1, 0), 2) +
                     pow(mHi[2] * R(2, 0), 2) + pow(mHi[3] * R(3, 0), 2);

  const double X22 = pow(mHi[0] * R(0, 1), 2) + pow(mHi[1] * R(1, 1), 2) +
                     pow(mHi[2] * R(2, 1), 2) + pow(mHi[3] * R(3, 1), 2);

  const double X12 = (R(0, 0) * pow(mHi[0], 2) * R(0, 1)) +
                     (R(1, 0) * pow(mHi[1], 2) * R(1, 1)) +
                     (R(2, 0) * pow(mHi[2], 2) * R(2, 1)) +
                     (R(3, 0) * pow(mHi[3], 2) * R(3, 1));

  const double X44 = pow(mHi[0] * R(0, 3), 2) + pow(mHi[1] * R(1, 3), 2) +
                     pow(mHi[2] * R(2, 3), 2) + pow(mHi[3] * R(3, 3), 2);

  const double X24 = (R(0, 1) * pow(mHi[0], 2) * R(0, 3)) +
                     (R(1, 1) * pow(mHi[1], 2) * R(1, 3)) +
                     (R(2, 1) * pow(mHi[2], 2) * R(2, 3)) +
                     (R(3, 1) * pow(mHi[3], 2) * R(3, 3));

  const double X33 = pow(mHi[0] * R(0, 2), 2) + pow(mHi[1] * R(1, 2), 2) +
                     pow(mHi[2] * R(2, 2), 2) + pow(mHi[3] * R(3, 2), 2);

  const double X13 = (R(0, 0) * pow(mHi[0], 2) * R(0, 2)) +
                     (R(1, 0) * pow(mHi[1], 2) * R(1, 2)) +
                     (R(2, 0) * pow(mHi[2], 2) * R(2, 2)) +
                     (R(3, 0) * pow(mHi[3], 2) * R(3, 2));

  const double X23 = (R(0, 1) * pow(mHi[0], 2) * R(0, 2)) +
                     (R(1, 1) * pow(mHi[1], 2) * R(1, 2)) +
                     (R(2, 1) * pow(mHi[2], 2) * R(2, 2)) +
                     (R(3, 1) * pow(mHi[3], 2) * R(3, 2));

  ///////////////////////////

  const double L1 = (cb * X11 - re_m12sq * sb) / (pow(v, 2) * pow(cb, 3));

  const double L2 = (sb * X22 - re_m12sq * cb) / (pow(v, 2) * pow(sb, 3));

  const double L3 = (2 * pow(mHp, 2) / pow(v, 2)) +
                    ((X12 - re_m12sq) / (pow(v, 2) * cb * sb));

  const double L4 =
      ((X44 - 2 * pow(mHp, 2)) / pow(v, 2)) + re_m12sq / (pow(v, 2) * cb * sb);

  const double re_L5 = (re_m12sq / (pow(v, 2) * sb * cb)) - (X44 / pow(v, 2));

  const double im_L5 = -(2 * X24) / (pow(v, 2) * cb);

  const double L6 = 4 * X33 / pow(vs, 2);

  const double L7 = 2 * X13 / (v * vs * cb);

  const double L8 = 2 * X23 / (v * vs * sb);

  return {L1, L2, L3, L4, re_L5, im_L5, L6, L7, L8};
}

/////Tadpole Conditions/////

double im_m12sq(double v, double tbeta, double im_L5) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double v1 = v * cb;
  const double v2 = v * sb;
  return (im_L5 * v1 * v2) / 2;
}

double M11sq(double v, double vs, double tbeta, double L1, double L3, double L4,
             double re_L5, double L7, double re_m12sq) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double v1 = v * cb;
  const double v2 = v * sb;
  return (-1.0 / 2.0 * L1 * pow(v1, 2)) -
         (1.0 / 2.0 * (L3 + L4 + re_L5) * pow(v2, 2)) - (L7 / 4 * pow(vs, 2)) +
         re_m12sq * v2 / v1;
  ;
}

double M22sq(double v, double vs, double tbeta, double L2, double L3, double L4,
             double re_L5, double L8, double re_m12sq) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double v1 = v * cb;
  const double v2 = v * sb;
  return (-1.0 / 2.0 * L2 * pow(v2, 2)) -
         (1.0 / 2.0 * (L3 + L4 + re_L5) * pow(v1, 2)) - (L8 / 4 * pow(vs, 2)) +
         re_m12sq * v1 / v2;
}

double Musq(double v, double vs, double L6, double L7, double L8, double mDMsq,
            double tbeta) {
  using std::atan;
  using std::cos;
  using std::pow;
  using std::sin;
  const double cb = cos(atan(tbeta));
  const double sb = sin(atan(tbeta));
  const double v1 = v * cb;
  const double v2 = v * sb;
  return (-L6 / 4) * pow(vs, 2) - (L7 / 2) * pow(v1, 2) -
         (L8 / 2) * pow(v2, 2) + mDMsq;
}

} // namespace

// Lambda equivalence:
// L1, L2, L3, L4, re_L5, im_L5, L6, L7, L8
// L[0], L[1], L[2], L[3], L[4], L[5], L[6], L[7], L[8]

CN2HDM::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(
          CN2HDMFourMasses({in.a1, in.a2, in.a3, in.a4, in.a5, in.a6}, in.tbeta,
                           in.mHa, in.mHb))},
      mHD{in.mHD}, mHp{in.mHp}, tbeta{in.tbeta},
      R{Utilities::OrderedMixMat4d(
          in.a1, in.a2, in.a3, in.a4, in.a5, in.a6,
          CN2HDMFourMasses({in.a1, in.a2, in.a3, in.a4, in.a5, in.a6}, in.tbeta,
                           in.mHa, in.mHb))},
      alpha{Utilities::MixMatAngles4d(R)}, L{Lambdas(tbeta, in.v, in.vs, mHi,
                                                     mHp, R, in.re_m12sq)},
      m12sq{in.re_m12sq, im_m12sq(in.v, tbeta, L[5])},
      m11sq{M11sq(in.v, in.vs, tbeta, L[0], L[2], L[3], L[4], L[7],
                  m12sq.real())},
      m22sq{M22sq(in.v, in.vs, tbeta, L[1], L[2], L[3], L[4], L[8],
                  m12sq.real())},
      musq{Musq(in.v, in.vs, L[6], L[7], L[8], std::pow(in.mHD, 2), in.tbeta)},
      vs{in.vs}, type{in.type}, v{in.v} {}

std::string CN2HDM::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  for (double m : mHi)
    printer << m;
  printer << mHD << mHp << tbeta;
  for (double a : alpha)
    printer << a;
  printer << m12sq.real() << m12sq.imag();
  printer << vs;
  printer << static_cast<int>(type);
  printer << v;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  for (double l : L)
    printer << l;
  printer << m11sq << m22sq;
  printer << musq;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}
// -------------------------------- Pheno --------------------------------

void CN2HDM::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};

  auto result = hdec.cn2hdm(
      static_cast<Yukawa>(p.type), SingletVev{p.vs}, TanBeta{p.tbeta},
      SquaredMassPar{p.m12sq.real()}, HcMass{p.mHp}, DarkAMass{p.mHD},
      HMassCPM{p.mHi[0]}, HMassCPM{p.mHi[1]}, HMassCPM{p.mHi[2]},
      HMassCPM{p.mHi[3]}, Lambda{p.L[0]}, Lambda{p.L[1]}, Lambda{p.L[2]},
      Lambda{p.L[3]}, Lambda{p.L[4]}, Lambda{p.L[6]},
      Lambda{p.L[7]}, Lambda{p.L[8]}, Lambda{p.L[5]}, MixAngle{p.alpha[0]},
      MixAngle{p.alpha[1]}, MixAngle{p.alpha[2]}, MixAngle{p.alpha[3]},
      MixAngle{p.alpha[4]}, MixAngle{p.alpha[5]});

  for (auto [key, value] : result) {
    p.data.Store(std::string(key), value);
  }
}

Interfaces::HiggsBoundsSignals::HBInput<CN2HDM::nHzero, CN2HDM::nHplus>
CN2HDM::HiggsBoundsInput(
    ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t i = 0; i != nHzero - 1; ++i) {
    hb.Mh(i) = p.mHi[i];
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    hb.CP_value(i) = 0;
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
    hb.BR_hjinvisible(i) = p.data["b_"s + namesHzero[i] + "_HDHD"];
  }
  //   // HiggsBounds_neutral_input_nonSMBR

  // decaying particle; decay products

  hb.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];
  hb.BR_hkhjhi(2, 0, 0) = p.data["b_H3_H1H1"];
  hb.BR_hkhjhi(3, 0, 0) = p.data["b_H4_H1H1"];
  hb.BR_hkhjhi(2, 1, 1) = p.data["b_H3_H2H2"];
  hb.BR_hkhjhi(3, 1, 1) = p.data["b_H4_H2H2"];
  hb.BR_hkhjhi(2, 0, 1) = p.data["b_H3_H1H2"];
  hb.BR_hkhjhi(2, 1, 0) = hb.BR_hkhjhi(2, 0, 1);
  hb.BR_hkhjhi(3, 0, 1) = p.data["b_H4_H1H2"];
  hb.BR_hkhjhi(3, 1, 0) = hb.BR_hkhjhi(3, 0, 1);
  hb.BR_hkhjhi(3, 0, 2) = p.data["b_H4_H1H3"];
  hb.BR_hkhjhi(3, 2, 0) = hb.BR_hkhjhi(3, 0, 2);
  hb.BR_hkhjhi(3, 1, 2) = p.data["b_H4_H2H3"];
  hb.BR_hkhjhi(3, 2, 1) = hb.BR_hkhjhi(3, 1, 2);
  hb.BR_hkhjhi(3, 2, 2) = p.data["b_H4_H3H3"];

  hb.BR_hjhiZ(1, 0) = p.data["b_H2_ZH1"];
  hb.BR_hjhiZ(2, 0) = p.data["b_H3_ZH1"];
  hb.BR_hjhiZ(2, 1) = p.data["b_H3_ZH2"];
  hb.BR_hjhiZ(3, 1) = p.data["b_H4_ZH2"];
  hb.BR_hjhiZ(3, 0) = p.data["b_H4_ZH1"];
  hb.BR_hjhiZ(3, 2) = p.data["b_H4_ZH3"];

  for (size_t i = 0; i != nHzero - 1; ++i) {
    hb.BR_hjHpiW(i, 0) = p.data["b_"s + namesHzero[i] + "_WpHm"];
    const double cVV = p.data["c_"s + namesHzero[i] + "VV"];
    const double cu_e = p.data["c_"s + namesHzero[i] + "uu_e"];
    const double cu_o = p.data["c_"s + namesHzero[i] + "uu_o"];
    const double cd_e = p.data["c_"s + namesHzero[i] + "dd_e"];
    const double cd_o = p.data["c_"s + namesHzero[i] + "dd_o"];
    const double cl_e = p.data["c_"s + namesHzero[i] + "ll_e"];
    const double cl_o = p.data["c_"s + namesHzero[i] + "ll_o"];

    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(cVV, 2);
    hb.XS_ee_bbhj_ratio(i) = pow(cd_e, 2) + pow(cd_o, 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(cl_e, 2) + pow(cl_o, 2);
    for (size_t j = i; j != nHzero - 1; ++j) {
      hb.XS_ee_hjhi_ratio(i, j) =
          pow(p.data["c_"s + namesHzero[i] + namesHzero[j] + "Z"], 2);
      hb.XS_ee_hjhi_ratio(j, i) = hb.XS_ee_hjhi_ratio(i, j);
    }
    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                          Tools::SushiTables::Collider::TEV);
    double bb =
        cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.TEV_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC7_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   Tevatron
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC8_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    //   Tevatron
    gg = cxnH0_.GG(p.mHi[i], cu_e, cd_e, cu_o, cd_o,
                   Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BB(p.mHi[i], cd_e, cd_o, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[i], cVV, {cu_e, cu_o}, {cd_e, cd_o});
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[i], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(cVV, 2);
    hb.LHC13_CS_tthj_ratio(i) =
        pow(cu_e, 2) + pow(cu_o, 2); //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(i) = pow(cu_e, 2) + pow(cu_o, 2); //< LO correct
  }
  hb.Mhplus(0) = p.mHp;
  hb.GammaTotal_Hpj(0) = p.data["w_Hp"];
  hb.CS_ee_HpjHmj_ratio(0) = 1;
  hb.BR_tWpb = p.data["b_t_Wb"];
  hb.BR_tHpjb(0) = p.data["b_t_Hpb"];
  hb.BR_Hpjcs(0) = p.data["b_Hp_cs"];
  hb.BR_Hpjcb(0) = p.data["b_Hp_cb"];
  hb.BR_Hpjtaunu(0) = p.data["b_Hp_taunu"];
  hb.BR_Hpjtb(0) = p.data["b_Hp_tb"];
  // BR_HpjWZ(0) == 0
  for (size_t i = 0; i != nHzero - 1; ++i)
    hb.BR_HpjhiW(0, i) = p.data["b_Hp_W"s + namesHzero[i]];

  // charged Higgs cxn (as of Jan 2020 the only one needed)
  auto coupsHp = TwoHDM::TwoHDMHpCoups(p.tbeta, p.type);
  p.data.Store("x_tHp", hbhs.GetHpCxn(p.mHp, coupsHp.rhot, coupsHp.rhob,
                                      p.data["b_t_Hpb"]));
  hb.LHC13_CS_Hpjtb(0) = p.data["x_tHp"];
  return hb;
}

Constraints::STUDetail::STUParameters
CN2HDM::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(2, nHzero + 1),
      Eigen::MatrixXd(2, nHplus + 1),
      {p.mHi[0], p.mHi[1], p.mHi[2], p.mHi[3], p.mHD},
      {p.mHp}};
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));
  res.mU << cb, -sb, sb, cb;

  res.mV(0, 0) = {0, cb};
  res.mV(0, 1) = {p.R(0, 0), -sb * p.R(0, 3)};
  res.mV(0, 2) = {p.R(1, 0), -sb * p.R(1, 3)};
  res.mV(0, 3) = {p.R(2, 0), -sb * p.R(2, 3)};
  res.mV(0, 4) = {p.R(3, 0), -sb * p.R(3, 3)};
  res.mV(0, 5) = {0, 0};

  res.mV(1, 0) = {0, sb};
  res.mV(1, 1) = {p.R(0, 1), cb * p.R(0, 3)};
  res.mV(1, 2) = {p.R(1, 1), cb * p.R(1, 3)};
  res.mV(1, 3) = {p.R(2, 1), cb * p.R(2, 3)};
  res.mV(1, 4) = {p.R(3, 1), cb * p.R(3, 3)};
  res.mV(1, 5) = {0, 0};

  return res;
}

bool CN2HDM::EWPValid(const ParameterPoint &p) {
  return (p.mHp + p.mHi[0] > Constants::mW) && (2 * p.mHi[0] > Constants::mZ) &&
         (2 * p.mHp > Constants::mZ);
}


std::map<std::string, double>
Models::CN2HDM::MOInput(const ParameterPoint &p) {
  std::string_view modelName;
  if (p.type == Yuk::typeII)
    modelName = "CN2HDM_T2";
  else
    modelName = "CN2HDM_T1";
  ScannerS::Interfaces::MicrOMEGAs::SelectModel(modelName);
  double a6 = std::asin(-p.R(0, 3));
  double a2 = std::asin(p.R(0, 2) / cos(a6));
  double a5 = std::asin(p.R(2, 3) / -cos(a6));
  double a1 = std::asin(p.R(0, 1) / (cos(a2) * cos(a6)));
  double a4 = std::asin(p.R(1, 3) / (-cos(a5) * cos(a6)));
  double a3 = std::asin((p.R(2, 0) * std::sin(a1) - p.R(2, 1) * std::cos(a1)) /
                        std::cos(a5));
  return {{"MZ", Constants::mZ},
          {"MW", Constants::mW},
          {"aEWM1", Constants::alphaAtMz},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"mHc", p.mHp},
          {"alph1", a1},
          {"alph2", a2},
          {"alph3", a3},
          {"alph4", a4},
          {"alph5", a5},
          {"alph6", a6},
          {"tbeta", p.tbeta},
          {"mH1", p.mHi[0]},
          {"mH2", p.mHi[1]},
          {"mHD", p.mHD},
          {"vs", p.vs},
          {"rem12sq", p.m12sq.real()},
          {"MC", Constants::mC},
          {"MB", Constants::mB},
          {"MT", Constants::mT},
          {"MM", Constants::mMu},
          {"MTA", Constants::mTau}};
}

double CN2HDM::CalcElectronEDM(ParameterPoint &p) {
  auto in = ScannerS::Tools::C2HEDMInput<nHzero - 1>{}; // Fermions not coupling
                                                        // to dark Higgs
  in.mHp = p.mHp;
  in.mHi = p.mHi;
  for (size_t i = 0; i != nHzero - 1; ++i) {
    in.c_Hff[i][0][0] = p.data["c_"s + namesHzero[i] + "uu_e"];
    in.c_Hff[i][0][1] = p.data["c_"s + namesHzero[i] + "uu_o"];
    in.c_Hff[i][1][0] = p.data["c_"s + namesHzero[i] + "dd_e"];
    in.c_Hff[i][1][1] = p.data["c_"s + namesHzero[i] + "dd_o"];
    in.c_Hff[i][2][0] = p.data["c_"s + namesHzero[i] + "ll_e"];
    in.c_Hff[i][2][1] = p.data["c_"s + namesHzero[i] + "ll_o"];
    in.c_HVV[i] = p.data["c_"s + namesHzero[i] + "VV"];
    in.c_HHpHm[i] = p.data["c_"s + namesHzero[i] + "HpHm"];
  }
  auto result = ScannerS::Tools::calcElectronEDM(in);
  p.data.Store("edm_e_f", result.contribF);
  p.data.Store("edm_e_Hp", result.contribHp);
  p.data.Store("edm_e_W", result.contribW);
  p.data.Store("edm_e_HpW", result.contribHpW);
  return result.value;
}

// ---------------------------------- Theory ----------------------------------

bool CN2HDM::BFB(const std::array<double, 9> &L) {
  using std::abs;
  using std::pow;
  using std::sqrt;

  // Ordering defined in header file. Here (and in Unitarity constraint def.) we
  // use L[0] = Lambda1 L[1] = Lambda2 L[2] = Lambda3 L[3] = Lambda4 L[4] =
  // Re_Lambda5 L[5] = Im_Lambda5 L[6] = Lambda6 L[7] = Lambda7 L[8] = Lambda8

  const double D = std::min(0., L[3] - abs(L[4]));

  const bool reg1 = (L[6] > 0) && (L[0] > 0) && (L[1] > 0) &&
                    (L[7] + std::sqrt(L[0] * L[6]) > 0) &&
                    (L[8] + std::sqrt(L[6] * L[1]) > 0) &&
                    (L[2] + D + std::sqrt(L[0] * L[1]) > 0) &&
                    (L[7] + L[8] * std::sqrt(L[2] / L[1]) >= 0);

  const bool reg2 = (L[6] > 0) && (L[0] > 0) && (L[1] > 0) &&
                    (std::sqrt(L[1] * L[6]) >= L[8]) &&
                    (L[8] > -std::sqrt(L[1] * L[6])) &&
                    (-std::sqrt(L[0] * L[6]) < L[7]) &&
                    (L[7] <= -L[8] * std::sqrt(L[2] / L[1])) &&
                    (L[6] * (L[2] + D) >
                     L[7] * L[8] - std::sqrt((L[0] * L[6] - pow(L[7], 2)) *
                                             (L[1] * L[6] - pow(L[8], 2))));

  return reg1 || reg2;
}

double CN2HDM::MaxUnitarityEV(const std::array<double, 9> &L) {
  // condition names refer to 1612.01309
  using ScannerS::Constants::pi;
  using std::abs;
  using std::pow;
  using std::sqrt;

  double L5tot = sqrt(pow(L[4], 2) + pow(L[5], 2));

  double maxEV = std::max({
      abs(1 / 2. *
          (L[0] + L[1] + sqrt(pow(L[0] - L[1], 2) + 4 * pow(L[3], 2)))), // b+
      abs(1 / 2. *
          (L[0] + L[1] + sqrt(pow(L[0] - L[1], 2) + 4 * pow(L5tot, 2)))), // c+
      abs(L[2] + 2 * L[3]) - 3 * L5tot,                                   // e1
      L[2] - L5tot,                                                       // e2
      abs(L[2] + 2 * L[3]) + 3 * L5tot,                                   // f+
      L[2] + L5tot,                                                       // f-
      L[2] + L[3],                                                        // f1
      abs(L[2] - L[3]),                                                   // p1
      abs(L[6]) / 4,                                                      // s1
      abs(L[7]) / 2,                                                      // s2
      abs(L[8]) / 2,                                                      // s3

  });
  // cubic polynomial of form x^3 + a x^2 + b x + c
  const double a = -24 * L[0] - 24 * L[1] - 4 * L[6];
  const double b = 576 * L[0] * L[1] - 256 * pow(L[2], 2) - 256 * L[2] * L[3] -
                   64 * pow(L[3], 2) + 96 * L[0] * L[6] - 6 + 96 * L[1] * L[6] -
                   6 - 32 * pow(L[7], 2) - 32 * pow(L[8], 2);
  const double c = -2304 * L[0] * L[1] * L[6] + 1024 * pow(L[2], 2) * L[6] +
                   1024 * L[2] * L[3] * L[6] + 256 * pow(L[3], 2) * L[6] +
                   768 * L[1] * pow(L[7], 2) - 1024 * L[2] * L[7] * L[8] -
                   512 * L[3] * L[7] * L[8] + 768 * L[0] * pow(L[8], 2);

  auto roots = Utilities::CubicRoots(a, b, c);
  return std::max(maxEV, 1 / 2. * Utilities::AbsMax(roots));
}

const Tools::SushiTables CN2HDM::cxnH0_{};

void CN2HDM::CalcCouplings(ParameterPoint &p) {
  const double cb = std::cos(std::atan(p.tbeta));
  const double sb = std::sin(std::atan(p.tbeta));

  // gauge couplings
  for (size_t i = 0; i != nHzero - 1; ++i) {
    p.data.Store("c_"s + namesHzero[i] + "VV", cb * p.R(i, 0) + sb * p.R(i, 1));
    for (size_t j = i; j != nHzero - 1; ++j) {
      // ScannerS uses effective coupling
      p.data.Store("c_"s + namesHzero[i] + namesHzero[j] + "Z",
                   (sb * p.R(i, 0) - cb * p.R(i, 1)) * p.R(j, 3) -
                       (sb * p.R(j, 0) - cb * p.R(j, 1)) * p.R(i, 3));
    }
  }

  // fermion couplings
  for (size_t i = 0; i != nHzero - 1; i++) {
    p.data.Store("c_"s + namesHzero[i] + "uu_e", p.R(i, 1) / sb);
    p.data.Store("c_"s + namesHzero[i] + "uu_o", -p.R(i, 3) / p.tbeta);

    if (p.type == Yuk::typeI) {
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", p.R(i, 3) / p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", p.R(i, 3) / p.tbeta);
    }
    if (p.type == Yuk::typeII) {
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", -p.R(i, 3) * p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", -p.R(i, 3) * p.tbeta);
    }
    if (p.type == Yuk::leptonSpecific) { // Lepton Specific
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", p.R(i, 3) / p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", -p.R(i, 3) * p.tbeta);
    }
    if (p.type == Yuk::flipped) { // flipped
      p.data.Store("c_"s + namesHzero[i] + "dd_e", p.R(i, 0) / cb);
      p.data.Store("c_"s + namesHzero[i] + "dd_o", -p.R(i, 3) * p.tbeta);
      p.data.Store("c_"s + namesHzero[i] + "ll_e", p.R(i, 1) / sb);
      p.data.Store("c_"s + namesHzero[i] + "ll_o", p.R(i, 3) / p.tbeta);
    }
  }

  // charged H+ H- Hi couplings
  // Dimensionless coupling c_hHpHm/v
  for (size_t i = 0; i != nHzero - 1; ++i) {
    p.data.Store(
        "c_"s + namesHzero[i] + "HpHm",
        +p.R(i, 0) * cb *
                ((p.L[4] - p.L[0] + p.L[3]) -
                 cb * cb * (p.L[4] - p.L[0] + p.L[2] + p.L[3])) +
            p.R(i, 1) * sb *
                (cb * cb * (p.L[4] - p.L[1] + p.L[2] + p.L[3]) - p.L[2]) +
            p.R(i, 2) * p.vs / p.v * 5 *
                (cb * cb * (p.L[7] - p.L[8]) - p.L[7]) -
            p.R(i, 3) * cb * sb * p.L[5]);
  }
}

void CN2HDM::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 0; i != nHzero - 1; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GG(p.mHi[i], p.data["c_"s + namesHzero[i] + "uu_e"],
                           p.data["c_"s + namesHzero[i] + "dd_e"],
                           p.data["c_"s + namesHzero[i] + "uu_o"],
                           p.data["c_"s + namesHzero[i] + "dd_o"],
                           Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BB(p.mHi[i], p.data["c_"s + namesHzero[i] + "dd_e"],
                           p.data["c_"s + namesHzero[i] + "dd_o"],
                           Tools::SushiTables::Collider::LHC13));
  }
}

} // namespace ScannerS::Models
