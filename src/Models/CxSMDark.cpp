#include "ScannerS/Models/CxSMDark.hpp"
#include "AnyHdecay.hpp"
#include "ScannerS/Constants.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <algorithm>
#include <cmath>
#include <map>
#include <sstream>
#include <utility>

namespace ScannerS::Models {

namespace {

//! mass ordered, {{c,s},{-s,c}}
double CalcSortedAlpha(double mHa, double mHb, double alphaIn) {
  using ScannerS::Constants::pi;
  if (mHa <= mHb)
    return alphaIn;
  else {
    if (sin(alphaIn) > 0)
      return alphaIn - pi / 2;
    else
      return alphaIn + pi / 2;
  }
}

// to Tizian's convention: {lambda, d2, delta2} --> {2lH, 2lS, 2lHS}
std::array<double, 3> QuarticCouplings(double mHl, double mHh, double alpha,
                                       double v, double vs) {
  using std::cos;
  using std::pow;
  using std::sin;
  double lambda =
      2 * (pow(mHh * sin(alpha), 2) + pow(mHl * cos(alpha), 2)) / pow(v, 2);
  double d2 =
      2 * (pow(mHh * cos(alpha), 2) + pow(mHl * sin(alpha), 2)) / pow(vs, 2);
  double delta2 =
      -2 * ((pow(mHh, 2) - pow(mHl, 2)) / (2 * v * vs)) * sin(2 * alpha);
  return {lambda, d2, delta2};
}

// msq = -muH^2 in Tizian's convention
double Msq(double v, double vs, const std::array<double, 3> &L) {
  return -(L[0] * pow(v, 2) + L[2] * pow(vs, 2)) / 2.0;
}

// b2 = -muS^2 in Tizian's convention
double B2(double v, double vs, const std::array<double, 3> &L, double b1) {
  return -L[1] / 2.0 * pow(vs, 2) - L[2] / 2.0 * pow(v, 2) - b1;
}

} // namespace

CxSMDark::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHl{std::min({in.mHa, in.mHb})}, mHh{std::max({in.mHa, in.mHb})},
      mHX{in.mHX}, alpha{CalcSortedAlpha(in.mHa, in.mHb, in.alpha)}, v{in.v},
      vs{in.vs}, L{QuarticCouplings(mHl, mHh, alpha, in.v, in.vs)},
      msq{Msq(in.v, in.vs, L)}, b1{-std::pow(in.mHX, 2)}, b2{B2(in.v, in.vs, L,
                                                                b1)} {}

void CxSMDark::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result =
      hdec.cxsmDark(MixAngle{p.alpha}, HMass{p.mHl}, HMass{p.mHh},
                    DarkAMass{p.mHX}, SingletVev{p.vs}, PotentialPar{0});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<CxSMDark::nHzero, CxSMDark::nHplus>
CxSMDark::HiggsBoundsInput(
    const ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  hb.Mh << p.mHl, p.mHh, p.mHX;
  hb.CP_value.setConstant(1);
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
    hb.BR_hjinvisible(i) = p.data["b_"s + namesHzero[i] + "_HDHD"];
  }

  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];

  for (size_t i = 0; i != nHzeroVisible; ++i) {
    // get the couplings
    const double kappa = p.data["R"s + namesHzero[i] + "1"];

    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(kappa, 2);
    hb.XS_ee_bbhj_ratio(i) = pow(kappa, 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(kappa, 2);

    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg =
        cxnH0_.GGe(hb.Mh(i), kappa, kappa, Tools::SushiTables::Collider::TEV);
    double bb = cxnH0_.BBe(hb.Mh(i), kappa, Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(hb.Mh(i), 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(hb.Mh(i), 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(kappa, 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(kappa, 2);
    hb.TEV_CS_vbf_ratio(i) = pow(kappa, 2);
    hb.TEV_CS_tthj_ratio(i) = pow(kappa, 2);      //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(kappa, 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(kappa, 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(hb.Mh(i), kappa, kappa, Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(hb.Mh(i), kappa, Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(hb.Mh(i), 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(hb.Mh(i), 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(kappa, 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(kappa, 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(kappa, 2);
    hb.LHC7_CS_tthj_ratio(i) = pow(kappa, 2);      //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(kappa, 2); //< LO correct
    hb.LHC7_CS_thj_schan_ratio(i) = pow(kappa, 2); //< LO correct
    //   LHC8
    gg = cxnH0_.GGe(hb.Mh(i), kappa, kappa, Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(hb.Mh(i), kappa, Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(hb.Mh(i), 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(hb.Mh(i), 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(kappa, 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(kappa, 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(kappa, 2);
    hb.LHC8_CS_tthj_ratio(i) = pow(kappa, 2);      //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(kappa, 2); //< LO correct
    hb.LHC8_CS_thj_schan_ratio(i) = pow(kappa, 2); //< LO correct
    //   LHC13
    gg =
        cxnH0_.GGe(hb.Mh(i), kappa, kappa, Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(hb.Mh(i), kappa, Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(hb.Mh(i), 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(hb.Mh(i), 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(hb.Mh(i), kappa, kappa, kappa);
    const auto x_VH_ref = hbhs.GetVHCxns(hb.Mh(i), 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(kappa, 2);
    hb.LHC13_CS_tthj_ratio(i) = pow(kappa, 2);      //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(kappa, 2); //< LO correct
    hb.LHC13_CS_thj_schan_ratio(i) = pow(kappa, 2); //< LO correct
    hb.LHC13_CS_tWhj_ratio(i) =
        Interfaces::HiggsBoundsSignals::tWHratio(kappa, kappa);
  }
  return hb;
}

Constraints::STUDetail::STUParameters
CxSMDark::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(1, nHzero + 1),
      Eigen::Matrix<double, 1, 1>::Constant(1),
      {p.mHl, p.mHh, p.mHX},
      {}};
  res.mV(0, 0) = {0, 1};
  res.mV(0, 1) = {std::cos(p.alpha), 0};
  res.mV(0, 2) = {-std::sin(p.alpha), 0};
  res.mV(0, 3) = {0, 0};
  return res;
}

std::map<std::string, double> CxSMDark::MOInput(const ParameterPoint &p) {
  return {{"muSd2", -p.b1},
          {"muH2", -p.msq},
          {"muS2", -p.b2},
          {"lH", 0.5 * p.L[0]},
          {"lHS", 0.5 * p.L[2]},
          {"lSDCx", 0.5 * p.L[1]},
          {"vs", p.vs},
          {"MZ", Constants::mZ},
          {"Gf", Constants::Gf},
          {"aS", Constants::alphaSAtMz},
          {"alfSMZ", Constants::alphaSAtMz},
          {"Mu2", Constants::mC},
          {"Md3", Constants::mB},
          {"Mu3", Constants::mT},
          {"Me2", Constants::mMu},
          {"Me3", Constants::mTau}};
}

const Tools::SushiTables CxSMDark::cxnH0_{};

std::string CxSMDark::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  printer << mHl << mHh << mHX;
  printer << alpha;
  for (double l : L)
    printer << l;
  printer << msq << b2 << b1;
  printer << v << vs;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void CxSMDark::CalcCouplings(ParameterPoint &p) {
  // kappas
  p.data.Store("R"s + namesHzero[0] + "1", std::cos(p.alpha));
  p.data.Store("R"s + namesHzero[1] + "1", -std::sin(p.alpha));
}

void CxSMDark::CalcCXNs(ParameterPoint &p) {
  const std::array<double, nHzeroVisible> mH{p.mHl, p.mHh};
  for (size_t i = 0; i != nHzeroVisible; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(mH[i], p.data["R"s + namesHzero[i] + "1"],
                            p.data["R"s + namesHzero[i] + "1"],
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store("x_"s + namesHzero[i] + "_bbH",
                 cxnH0_.BBe(mH[i], p.data["R"s + namesHzero[i] + "1"],
                            Tools::SushiTables::Collider::LHC13));
  }
}

std::vector<double> CxSMDark::BsmptInput(const ParameterPoint &p) {
  return {p.v, p.vs, 0., p.msq, p.L[0], p.L[2], p.b2, p.L[1], p.b1, 0., 0., 0.};
}

} // namespace ScannerS::Models
