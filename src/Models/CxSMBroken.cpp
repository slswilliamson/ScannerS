#include "ScannerS/Models/CxSMBroken.hpp"

#include "AnyHdecay.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Interfaces/HiggsBoundsSignals.hpp"
#include "ScannerS/Tools/SushiTables.hpp"
#include "ScannerS/Utilities.hpp"
#include <cmath>
#include <map>
#include <sstream>
#include <utility>

namespace ScannerS::Models {

// -------------------------------- Generation --------------------------------
namespace {
double CxSMBrokenThirdMass(const CxSMBroken::AngleInput &in) {
  using std::cos;
  using std::sin;
  using std::sqrt;
  const double mH1sq = in.mHa * in.mHa;
  const double mH2sq = in.mHb * in.mHb;
  const double mH3sq =
      (mH1sq * mH2sq *
       (-(cos(in.a3) * sin(in.a1) * sin(in.a2)) - cos(in.a1) * sin(in.a3)) *
       (-(cos(in.a1) * cos(in.a3) * sin(in.a2)) + sin(in.a1) * sin(in.a3))) /
      (-(mH2sq * cos(in.a1) * pow(cos(in.a2), 2) * sin(in.a1)) +
       mH1sq *
           (cos(in.a3) * sin(in.a1) + cos(in.a1) * sin(in.a2) * sin(in.a3)) *
           (cos(in.a1) * cos(in.a3) - sin(in.a1) * sin(in.a2) * sin(in.a3)));
  if (mH3sq >= 0)
    return sqrt(mH3sq);
  return -1;
}

double Va(const std::array<double, 3> &mHi, const Eigen::Matrix3d &R,
          double vs) {
  return (mHi[0] * mHi[0] * R(0, 2) * R(1, 1) * vs -
          mHi[1] * mHi[1] * R(0, 1) * R(1, 2) * vs) /
         (R(0, 1) * R(1, 1) * (mHi[0] * mHi[0] - mHi[1] * mHi[1]));
}

std::array<double, 3> QuarticCouplings(const std::array<double, 3> &mHi,
                                       const Eigen::Matrix3d &R, double v,
                                       double vs, double va) {
  using std::pow;
  return {(2 * (mHi[0] * mHi[0] * pow(R(0, 0), 2) +
                mHi[1] * mHi[1] * pow(R(1, 0), 2) +
                mHi[2] * mHi[2] * pow(R(2, 0), 2))) /
              pow(v, 2),
          (2 * (mHi[0] * mHi[0] * pow(R(0, 2), 2) +
                mHi[1] * mHi[1] * pow(R(1, 2), 2) +
                mHi[2] * mHi[2] * pow(R(2, 2), 2))) /
              pow(va, 2),
          (2 * (mHi[0] * mHi[0] * R(0, 0) * R(0, 1) +
                mHi[1] * mHi[1] * R(1, 0) * R(1, 1) +
                mHi[2] * mHi[2] * R(2, 0) * R(2, 1))) /
              (v * vs)};
}

double A1(const std::array<double, 3> &mHi, const Eigen::Matrix3d &R, double vs,
          const std::array<double, 3> &L) {
  using std::sqrt;
  return (vs * (-2 * mHi[0] * mHi[0] * pow(R(0, 1), 2) -
                2 * mHi[1] * mHi[1] * pow(R(1, 1), 2) -
                2 * mHi[2] * mHi[2] * pow(R(2, 1), 2) + L[1] * pow(vs, 2))) /
         (2. * sqrt(2));
}

double Msq(double v, double vs, double va, const std::array<double, 3> &L) {
  return (-(L[0] * pow(v, 2)) - L[2] * pow(va, 2) - L[2] * pow(vs, 2)) / 2.;
}

double B2(double v, double vs, double va, const std::array<double, 3> &L,
          double a1) {
  return -(2 * sqrt(2) * a1 + L[2] * pow(v, 2) * vs + L[1] * pow(va, 2) * vs +
           L[1] * pow(vs, 3)) /
         (2. * vs);
}

} // namespace

CxSMBroken::ParameterPoint::ParameterPoint(const AngleInput &in)
    : mHi{Utilities::Sorted(
          std::array<double, 3>({in.mHa, in.mHb, CxSMBrokenThirdMass(in)}))},
      R(Utilities::OrderedMixMat3d(in.a1, in.a2, in.a3,
                                   {in.mHa, in.mHb, CxSMBrokenThirdMass(in)})),
      alpha{Utilities::MixMatAngles3d(R)}, v{in.v}, vs{in.vs},
      va{Va(mHi, R, vs)}, L{QuarticCouplings(mHi, R, v, vs, va)}, a1{A1(mHi, R,
                                                                        vs, L)},
      msq{Msq(v, vs, va, L)}, b1{-((std::sqrt(2) * a1) / vs)}, b2{B2(v, vs, va,
                                                                     L, a1)} {}

// -------------------------------- Pheno --------------------------------
void CxSMBroken::RunHdecay(ParameterPoint &p) {
  using namespace AnyHdecay;
  static const Hdecay hdec{};
  auto result = hdec.cxsmBroken(MixAngle{p.alpha[0]}, MixAngle{p.alpha[1]},
                                MixAngle{p.alpha[2]}, HMass{p.mHi[0]},
                                HMass{p.mHi[2]}, SingletVev{p.vs});
  for (auto [key, value] : result)
    p.data.Store(std::string(key), value);
}

Interfaces::HiggsBoundsSignals::HBInput<CxSMBroken::nHzero, CxSMBroken::nHplus>
CxSMBroken::HiggsBoundsInput(
    const ParameterPoint &p,
    const Interfaces::HiggsBoundsSignals::HiggsBoundsSignals<nHzero, nHplus>
        &hbhs) {
  Interfaces::HiggsBoundsSignals::HBInput<nHzero, nHplus> hb;
  for (size_t i = 0; i != nHzero; ++i) {
    hb.Mh(i) = p.mHi[i];
    hb.GammaTotal_hj(i) = p.data["w_"s + namesHzero[i]];
    hb.CP_value(i) = 1;
    // HiggsBounds_neutral_input_SMBR
    hb.BR_hjss(i) = p.data["b_"s + namesHzero[i] + "_ss"];
    hb.BR_hjcc(i) = p.data["b_"s + namesHzero[i] + "_cc"];
    hb.BR_hjbb(i) = p.data["b_"s + namesHzero[i] + "_bb"];
    hb.BR_hjtt(i) = p.data["b_"s + namesHzero[i] + "_tt"];
    hb.BR_hjmumu(i) = p.data["b_"s + namesHzero[i] + "_mumu"];
    hb.BR_hjtautau(i) = p.data["b_"s + namesHzero[i] + "_tautau"];
    hb.BR_hjWW(i) = p.data["b_"s + namesHzero[i] + "_WW"];
    hb.BR_hjZZ(i) = p.data["b_"s + namesHzero[i] + "_ZZ"];
    hb.BR_hjZga(i) = p.data["b_"s + namesHzero[i] + "_Zgam"];
    hb.BR_hjgaga(i) = p.data["b_"s + namesHzero[i] + "_gamgam"];
    hb.BR_hjgg(i) = p.data["b_"s + namesHzero[i] + "_gg"];
  }

  // HiggsBounds_neutral_input_nonSMBR
  hb.BR_hkhjhi(1, 0, 0) = p.data["b_H2_H1H1"];
  hb.BR_hkhjhi(2, 0, 0) = p.data["b_H3_H1H1"];
  hb.BR_hkhjhi(2, 0, 1) = p.data["b_H3_H1H2"];
  hb.BR_hkhjhi(2, 1, 1) = p.data["b_H3_H2H2"];
  hb.BR_hkhjhi(2, 1, 0) = hb.BR_hkhjhi(2, 0, 1);

  for (size_t i = 0; i != nHzero; ++i) {

    // HiggsBounds_neutral_input_LEP
    hb.XS_ee_hjZ_ratio(i) = pow(p.R(i, 0), 2);
    hb.XS_ee_bbhj_ratio(i) = pow(p.R(i, 0), 2);
    hb.XS_ee_tautauhj_ratio(i) = pow(p.R(i, 0), 2);

    // HiggsBounds_neutral_input_hadr
    //   Tevatron
    double gg = cxnH0_.GGe(p.mHi[i], p.R(i, 0), p.R(i, 0),
                           Tools::SushiTables::Collider::TEV);
    double bb =
        cxnH0_.BBe(p.mHi[i], p.R(i, 0), Tools::SushiTables::Collider::TEV);
    double gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::TEV);
    double bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::TEV);
    hb.TEV_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.TEV_CS_gg_hj_ratio(i) = gg / gg0;
    hb.TEV_CS_bb_hj_ratio(i) = bb / bb0;
    hb.TEV_CS_hjW_ratio(i) = pow(p.R(i, 0), 2);
    hb.TEV_CS_hjZ_ratio(i) = pow(p.R(i, 0), 2);
    hb.TEV_CS_vbf_ratio(i) = pow(p.R(i, 0), 2);
    hb.TEV_CS_tthj_ratio(i) = pow(p.R(i, 0), 2); //< neglects ZH>ttH diagrams
    hb.TEV_CS_thj_tchan_ratio(i) = pow(p.R(i, 0), 2); //< LO correct
    hb.TEV_CS_thj_schan_ratio(i) = pow(p.R(i, 0), 2); //< LO correct
    //   LHC7
    gg = cxnH0_.GGe(p.mHi[i], p.R(i, 0), p.R(i, 0),
                    Tools::SushiTables::Collider::LHC7);
    bb = cxnH0_.BBe(p.mHi[i], p.R(i, 0), Tools::SushiTables::Collider::LHC7);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC7);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC7);
    hb.LHC7_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC7_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC7_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC7_CS_hjW_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC7_CS_hjZ_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC7_CS_vbf_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC7_CS_tthj_ratio(i) = pow(p.R(i, 0), 2); //< neglects ZH>ttH diagrams
    hb.LHC7_CS_thj_tchan_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC7_CS_thj_schan_ratio(i) = pow(p.R(i, 0), 2);
    //   LHC8
    gg = cxnH0_.GGe(p.mHi[i], p.R(i, 0), p.R(i, 0),
                    Tools::SushiTables::Collider::LHC8);
    bb = cxnH0_.BBe(p.mHi[i], p.R(i, 0), Tools::SushiTables::Collider::LHC8);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC8);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC8);
    hb.LHC8_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC8_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC8_CS_bb_hj_ratio(i) = bb / bb0;
    hb.LHC8_CS_hjW_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC8_CS_hjZ_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC8_CS_vbf_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC8_CS_tthj_ratio(i) = pow(p.R(i, 0), 2); //< neglects ZH>ttH diagrams
    hb.LHC8_CS_thj_tchan_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC8_CS_thj_schan_ratio(i) = pow(p.R(i, 0), 2);
    //   LHC13
    gg = cxnH0_.GGe(p.mHi[i], p.R(i, 0), p.R(i, 0),
                    Tools::SushiTables::Collider::LHC13);
    bb = cxnH0_.BBe(p.mHi[i], p.R(i, 0), Tools::SushiTables::Collider::LHC13);
    gg0 = cxnH0_.GGe(p.mHi[i], 1, 1, Tools::SushiTables::Collider::LHC13);
    bb0 = cxnH0_.BBe(p.mHi[i], 1, Tools::SushiTables::Collider::LHC13);
    hb.LHC13_CS_hj_ratio(i) = (gg + bb) / (gg0 + bb0);
    hb.LHC13_CS_gg_hj_ratio(i) = gg / gg0;
    hb.LHC13_CS_bb_hj_ratio(i) = bb / bb0;
    const auto x_VH = hbhs.GetVHCxns(p.mHi[i], p.R(i, 0), p.R(i, 0), p.R(i, 0));
    const auto x_VH_ref = hbhs.GetVHCxns(p.mHi[i], 1, 1, 1);
    hb.LHC13_CS_hjZ_ratio(i) = x_VH.x_hZ / x_VH_ref.x_hZ;
    hb.LHC13_CS_gg_hjZ_ratio(i) = x_VH.x_gg_hZ / x_VH_ref.x_gg_hZ;
    hb.LHC13_CS_qq_hjZ_ratio(i) = x_VH.x_qq_hZ / x_VH_ref.x_qq_hZ;
    hb.LHC13_CS_hjW_ratio(i) = x_VH.x_hW / x_VH_ref.x_hW;
    hb.LHC13_CS_vbf_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC13_CS_tthj_ratio(i) = pow(p.R(i, 0), 2); //< neglects ZH>ttH diagrams
    hb.LHC13_CS_thj_tchan_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC13_CS_thj_schan_ratio(i) = pow(p.R(i, 0), 2);
    hb.LHC13_CS_tWhj_ratio(i) =
        Interfaces::HiggsBoundsSignals::tWHratio(p.R(i, 0), p.R(i, 0));
  }
  return hb;
}

Constraints::STUDetail::STUParameters
CxSMBroken::STUInput(const ParameterPoint &p) {
  Constraints::STUDetail::STUParameters res{
      Eigen::MatrixXcd(1, nHzero + 1),
      Eigen::Matrix<double, 1, 1>::Constant(1),
      {p.mHi[0], p.mHi[1], p.mHi[2]},
      {}};
  res.mV(0, 0) = {0, 1};
  res.mV(0, 1) = {p.R(0, 0), 0};
  res.mV(0, 2) = {p.R(1, 0), 0};
  res.mV(0, 3) = {p.R(2, 0), 0};
  return res;
}

const Tools::SushiTables CxSMBroken::cxnH0_{};

std::string CxSMBroken::ParameterPoint::ToString() const {
  std::ostringstream os;
  auto printer = Utilities::TSVPrinter(os);
  for (auto m : mHi)
    printer << m;
  printer << R.format(Utilities::TSVPrinter::matrixFormat);
  for (double a : alpha)
    printer << a;
  for (double l : L)
    printer << l;
  printer << msq << b2 << b1 << a1;
  printer << v << vs << va;
  for (const auto &[key, value] : data)
    printer << value;
  return os.str();
}

void CxSMBroken::CalcCXNs(ParameterPoint &p) {
  for (size_t i = 0; i != nHzero; ++i) {
    p.data.Store("x_"s + namesHzero[i] + "_ggH",
                 cxnH0_.GGe(p.mHi[i], p.R(i, 0), p.R(i, 0),
                            Tools::SushiTables::Collider::LHC13));
    p.data.Store(
        "x_"s + namesHzero[i] + "_bbH",
        cxnH0_.BBe(p.mHi[i], p.R(i, 0), Tools::SushiTables::Collider::LHC13));
  }
}

std::vector<double> CxSMBroken::BsmptInput(const ParameterPoint &p) {
  return {p.v,  p.vs,   p.va, p.msq, p.L[0], p.L[2],
          p.b2, p.L[1], p.b1, 0.,    p.a1,   0.};
}

} // namespace ScannerS::Models
