#include "ScannerS/Tools/CN2HEDM.hpp"
#include "ScannerS/Constants.hpp"
#include <algorithm>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <limits>
#include <stdexcept>
#include <iostream>

namespace ScannerS::Tools {

namespace {
//! 1st generation fermion masses
constexpr std::array<double, 3> mLightF{Constants::mU, Constants::mD,
                                        Constants::mE};
//! squared 3rd gen fermion masses
constexpr std::array<double, 3> mFsq{Constants::mT * Constants::mT,
                                     Constants::mB *Constants::mB,
                                     Constants::mTau *Constants::mTau};
// arrays of the above quantities, 0 = up-type, 1 = down-type, 2 =
// charged leptons
constexpr std::array<double, 3> Q{Constants::Qu, Constants::Qd,
                                  Constants::Ql}; //< EM quantum numbers
//! fermion couplings to a photon
constexpr std::array<double, 3> c_ffgam{2 * Constants::Qu * Constants::e,
                                        2 * Constants::Qd *Constants::e,
                                        2 * Constants::Ql *Constants::e};
//! up-type quark coupling to Z
constexpr double c_uuZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3u - 2 * Constants::s2tw * Constants::Qu);
//! down-type quark coupling to Z
constexpr double c_ddZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3d - 2 * Constants::s2tw * Constants::Qd);
//! charged lepton coupling to Z
constexpr double c_llZ = Constants::e / Constants::stw / Constants::ctw *
                         (Constants::T3l - 2 * Constants::s2tw * Constants::Ql);
//! fermion couplings to a Z
constexpr std::array<double, 3> c_ffZ{c_uuZ, c_ddZ, c_llZ};
constexpr std::array<int, 3> Nc{3, 3, 1}; //< number of colors
} // namespace

ElectronEDM CN2HEDM::operator()(const CN2HEDMInput &in) const {
  auto mHisq = std::array<double, 4>{};
  std::transform(in.mHi.begin(), in.mHi.end(), mHisq.begin(),
                 [](double m) { return m * m; });

  ElectronEDM res;
  res.contribF = edm_F(2, in.c_Hff, mHisq); // 2:  which fermion to calculate for (up: 0, down: 1, e: 2)
  res.contribHp = edm_Hp(2, in.c_Hff, in.c_HHpHm, mHisq, in.mHp * in.mHp);
  res.contribW = edm_W(2, in.c_Hff, in.c_HVV, mHisq);
  res.contribHpW =
      edm_HW(2, in.c_Hff, in.c_HVV, in.c_HHpHm, mHisq, in.mHp * in.mHp);
  res.value = res.contribF + res.contribHp + res.contribW + res.contribHpW;
  return res;
}

double CN2HEDM::edm_F(
    size_t iferm,
    const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
    const std::array<double, 4> &mHisq) const {
  using Constants::mZsq;
  double result = 0;

  for (size_t f = 0; f != 3; f++) {   // iterate over top, bottom, tau
    for (size_t h = 0; h != 4; h++) { // iterate over neutral Higgses
      double temp = 0;
      // gammagamma contribution
      temp += c_Hi_ffj_cpk[h][iferm][1] * c_Hi_ffj_cpk[h][f][0] * c_ffgam[f] *
              mFsq[f] / mHisq[h] * (-1) * func_I(mFsq[f], mHisq[h], 1);
      temp += c_Hi_ffj_cpk[h][iferm][0] * c_Hi_ffj_cpk[h][f][1] * c_ffgam[f] *
              mFsq[f] / mHisq[h] * (-1) * func_I(mFsq[f], mHisq[h], 2);
      result += Nc[f] * Q[f] * c_ffgam[iferm] * temp;
      // Zgamma contribution
      temp = 0;
      temp += c_Hi_ffj_cpk[h][iferm][1] * c_Hi_ffj_cpk[h][f][0] * c_ffZ[f] *
              mFsq[f] / (mHisq[h] - mZsq) *
              (func_I(mFsq[f], mZsq, 1) - func_I(mFsq[f], mHisq[h], 1));
      temp += c_Hi_ffj_cpk[h][iferm][0] * c_Hi_ffj_cpk[h][f][1] * c_ffZ[f] *
              mFsq[f] / (mHisq[h] - mZsq) *
              (func_I(mFsq[f], mZsq, 2) - func_I(mFsq[f], mHisq[h], 2));
      result += Nc[f] * Q[f] * c_ffZ[iferm] * temp;
    }
  }
  return -result * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double CN2HEDM::edm_Hp(
    size_t iferm,
    const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
    const std::array<double, 4> &c_Hi_HpHm, const std::array<double, 4> &mHisq,
    double mHpsq) const {
  using Constants::mZsq;
  using Constants::vEW;
  //! 2HDM H+H-Z coupling
  constexpr double c_HpHmZ{1 / 2. * Constants::e / Constants::stw /
                           Constants::ctw * (1 - 2 * Constants::s2tw)};

  double result = 0;

  for (size_t h = 0; h != 4; h++) { // iterate over neutral Higgses
    double temp = 0;
    // gammagamma contribution
    temp += c_ffgam[iferm] * (-1 / 2.) * Constants::e * vEW * vEW / mHisq[h] *
            (-func_I(mHpsq, mHisq[h], 1) + func_I(mHpsq, mHisq[h], 2));
    // Zgamma contribution
    temp += c_ffZ[iferm] * (-1 / 2.) * c_HpHmZ * vEW * vEW / (mHisq[h] - mZsq) *
            (func_I(mHpsq, mZsq, 1) - func_I(mHpsq, mHisq[h], 1) -
             func_I(mHpsq, mZsq, 2) + func_I(mHpsq, mHisq[h], 2));
    temp *= c_Hi_ffj_cpk[h][iferm][1] * c_Hi_HpHm[h];
    result += temp;
  }
  return result * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double CN2HEDM::edm_W(
    size_t iferm,
    const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
    const std::array<double, 4> &c_Hi_VV,
    const std::array<double, 4> &mHisq) const {
  using Constants::mWsq, Constants::mZsq;
  //! WWZ triple gauge coupling
  constexpr double c_WWZ = Constants::e / Constants::stw * Constants::ctw;
  double result = 0;

  for (size_t h = 0; h != 4; h++) {
    double temp = 0;
    // gammagamma contribution
    temp += c_ffgam[iferm] * Constants::e * 2 * mWsq / (mHisq[h]) *
            (-1 / 4. * (6 + mHisq[h] / mWsq) * func_I(mWsq, mHisq[h], 1) +
             (-4 + 1 / 4. * (6 + mHisq[h] / mWsq)) * func_I(mWsq, mHisq[h], 2));
    // Zgamma contribution
    temp +=
        c_ffZ[iferm] * c_WWZ * (2 * mWsq) / (mHisq[h] - mZsq) *
        (-1 / 4. *
             ((6 - mZsq / mWsq) + (1 - mZsq / (2 * mWsq)) * mHisq[h] / mWsq) *
             (func_I(mWsq, mHisq[h], 1) - func_I(mWsq, mZsq, 1)) +
         ((-4 + mZsq / mWsq) +
          1 / 4. *
              (6 - mZsq / mWsq + (1 - mZsq / (2 * mWsq)) * mHisq[h] / mWsq)) *
             (func_I(mWsq, mHisq[h], 2) - func_I(mWsq, mZsq, 2)));
    result += temp * c_Hi_ffj_cpk[h][iferm][1] * c_Hi_VV[h];
  }
  return result * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double CN2HEDM::edm_HW(
    size_t iferm,
    const std::array<std::array<std::array<double, 2>, 3>, 4> &c_Hi_ffj_cpk,
    const std::array<double, 4> &c_Hi_VV,
    const std::array<double, 4> &c_Hi_HpHm, const std::array<double, 4> &mHisq,
    double mHpsq) const {
  using Constants::mWsq, Constants::mZsq;
  constexpr std::array<int, 3> Sx{-1, 1, 1}; //!< Eq. (4.9)

  double result = 0;

  for (size_t h = 0; h != 4; h++) {
    result +=
        c_Hi_ffj_cpk[h][iferm][1] * c_Hi_VV[h] * std::pow(Constants::e, 2) /
        2. / Constants::s2tw * mWsq / (mHpsq - mWsq) *
        (func_I(mWsq, mHisq[h], 4, mHpsq) - func_I(mHpsq, mHisq[h], 4, mHpsq));
    result +=
        c_Hi_ffj_cpk[h][iferm][1] * c_Hi_HpHm[h] * mWsq / (mHpsq - mWsq) *
        (func_I(mWsq, mHisq[h], 5, mHpsq) - func_I(mHpsq, mHisq[h], 5, mHpsq));
  }
  return -result * Sx[iferm] * mLightF[iferm] * sqrt(2) * Constants::Gf /
         pow(4 * Constants::pi, 4) * Constants::invGeVToCm;
}

double CN2HEDM::func_I(double m1sq, double m2sq, int i, double mHpsq) const {
  double result, error;

  struct iparam {
    double m1sq;
    double m2sq;
    double mHpsq;
    double mWsq;
  };
  iparam Aparam{m1sq, m2sq, mHpsq, Constants::mWsq};

  // Assign gsl function to integrate
  gsl_function F;

  switch (i) {
  case 1:
    F.function = [](double x, void *params) {
      iparam *A{static_cast<iparam *>(params)};

      return (1 - 2 * x * (1 - x)) *
             (A->m2sq / (A->m1sq - A->m2sq * x * (1 - x)) *
              log(A->m2sq * x * (1 - x) / A->m1sq));
    };
    break;

  case 2:
    F.function = [](double x, void *params) {
      iparam *A{static_cast<iparam *>(params)};

      return A->m2sq / (A->m1sq - A->m2sq * x * (1 - x)) *
             log(A->m2sq * x * (1 - x) / A->m1sq);
    };
    break;

  case 4:
    F.function = [](double x, void *params) {
      iparam *A{static_cast<iparam *>(params)};

      return (x * pow(1 - x, 2) - 4 * pow(1 - x, 2) +
              (A->mHpsq - A->m2sq) / A->mWsq * x * pow(1 - x, 2)) *
             (A->m1sq /
              (A->mWsq * (1 - x) + A->m2sq * x - A->m1sq * x * (1 - x))) *
             log((A->mWsq * (1 - x) + A->m2sq * x) / (A->m1sq * x * (1 - x)));
    };
    break;

  case 5:
    F.function = [](double x, void *params) {
      iparam *A{static_cast<iparam *>(params)};

      return 2 * (A->m1sq * x * pow(1 - x, 2)) /
             (A->mHpsq * (1 - x) + A->m2sq * x - A->m1sq * x * (1 - x)) *
             log((A->mHpsq * (1 - x) + A->m2sq * x) / (A->m1sq * x * (1 - x)));
    };
    break;

  default:
    throw(std::runtime_error("Invalid loop integral requested in CN2HEDM!"));
    break;
  }
  F.params = &Aparam;

  // toggle gsl error handler off
  gsl_error_handler_t *backup_handler = gsl_set_error_handler_off();
  // perform the integration using an adaptive method
  int status = gsl_integration_qags(&F, 0, 1, 0, 1e-10, 100, integrator.get(),
                                    &result, &error);

  // try again using less precision (still much more than necessary)
  if (status != GSL_SUCCESS) {
    status = gsl_integration_qags(&F, 0, 1, 0, 1e-7, 100, integrator.get(),
                                  &result, &error);
  }
  // toggle error handler back on
  gsl_set_error_handler(backup_handler);
  // if something went wrong, return inf
  if (status != GSL_SUCCESS) {
    return std::numeric_limits<double>::infinity();
    // throw(std::runtime_error("Numeric integration in CN2HEDM loop integral
    // returned GSL error status: " + std::string(gsl_strerror(status))));
  }
  return result;
}

} // namespace ScannerS::Tools
