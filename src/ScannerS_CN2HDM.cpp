#include "ScannerS/Constraints/BFB.hpp"
#include "ScannerS/Constraints/BPhysics.hpp"
#include "ScannerS/Constraints/Constraint.hpp"
#include "ScannerS/Constraints/DarkMatter.hpp"
#include "ScannerS/Constraints/ElectronEDM.hpp"
#include "ScannerS/Constraints/Higgs.hpp"
#include "ScannerS/Constraints/STU.hpp"
#include "ScannerS/Constraints/Unitarity.hpp"
#include "ScannerS/Core.hpp"
#include "ScannerS/Models/CN2HDM.hpp"
#ifdef BSMPT_FOUND
#include "ScannerS/Constraints/EWPT.hpp"
#endif

using namespace ScannerS;

int main(int argc, char *argv[]) {
  using Model = Models::CN2HDM;
  ScannerSSetup<Model> scanners(argc, argv);
  scanners.AddParameters({"mHa", "mHb", "mHD", "mHp", "a1", "a2", "a3", "a4", "a5", "a6", "tbeta", "re_m12sq", "vs", "type"});
  scanners.AddConstraints<Constraints::BFB, Constraints::Unitarity, Constraints::BPhysics, Constraints::STU, Constraints::ElectronEDM, Constraints::Higgs>();
#ifdef BSMPT_FOUND
  scanners.AddConstraints<Constraints::EWPT>();
#endif

#ifdef MicrOMEGAs_FOUND
  scanners.AddConstraints<Constraints::DarkMatter>();
#endif

  auto mode = scanners.Parse();
  auto out = scanners.GetOutput();

  auto bfb = scanners.GetConstraint<Constraints::BFB>();
  auto uni = scanners.GetConstraint<Constraints::Unitarity>();
  auto bphys = scanners.GetConstraint<Constraints::BPhysics>();
  auto stu = scanners.GetConstraint<Constraints::STU>();
  auto edm = scanners.GetConstraint<Constraints::ElectronEDM>();
  auto higgs =
      scanners.GetConstraint<Constraints::Higgs>(Constants::chisq2Sigma2d);
#ifdef BSMPT_FOUND
  auto ewpt = scanners.GetConstraint<Constraints::EWPT>();
#endif
#ifdef MicrOMEGAs_FOUND
  auto dm = scanners.GetConstraint<Constraints::DarkMatter>();
#endif

  /////////// SCAN MODE ///////////

  scanners.PrintConfig(mode);
  switch (mode) {
  case RunMode::scan: {
    auto mHa = scanners.GetDoubleParameter("mHa");
    auto mHb = scanners.GetDoubleParameter("mHb");
    auto mHD = scanners.GetDoubleParameter("mHD");
    auto mHp = scanners.GetDoubleParameter("mHp");
    auto a1 = scanners.GetDoubleParameter("a1");
    auto a2 = scanners.GetDoubleParameter("a2");
    auto a3 = scanners.GetDoubleParameter("a3");
    auto a4 = scanners.GetDoubleParameter("a4");
    auto a5 = scanners.GetDoubleParameter("a5");
    auto a6 = scanners.GetDoubleParameter("a6");
    auto tbeta = scanners.GetDoubleParameter("tbeta");
    auto re_m12sq = scanners.GetDoubleParameter("re_m12sq");
    auto vs = scanners.GetDoubleParameter("vs");
    auto type = scanners.GetIntParameter("type");

    size_t n = 0;
    while (n < scanners.npoints) {
      Model::AngleInput in{
          mHa(scanners.rGen),   mHb(scanners.rGen),
          mHD(scanners.rGen),   mHp(scanners.rGen),
          a1(scanners.rGen),    a2(scanners.rGen),
          a3(scanners.rGen),    a4(scanners.rGen),
          a5(scanners.rGen),    a6(scanners.rGen),
          tbeta(scanners.rGen), re_m12sq(scanners.rGen),
          vs(scanners.rGen),    static_cast<Model::Yuk>(type(scanners.rGen)),
          Constants::vEW};
      Model::ParameterPoint p(in);
      if (Model::Valid(p) && uni(p) && bfb(p) && bphys(p) && stu(p)) {
        Model::CalcCouplings(p); // now we need the couplings
        if (edm(p)) {
          Model::RunHdecay(p); // for higgs we need the BR
          if (higgs(p)
#ifdef MicrOMEGAs_FOUND
              && dm(p)
#endif
          ) {
            Model::CalcCXNs(p); // we want the 13TeV cxns in the output
            out(p, n++);
          }
        }
      }
    }
    return 0;
  } //Scan Mode

  case RunMode::check: {
    auto points =
        scanners.GetInput({"mH1", "mH2", "mHD", "mHp", "a1", "a2", "a3", "a4", "a5", "a6", "tbeta", "re_m12sq", "vs", "yuktype"});

    std::vector<double> param;
    std::string pId;
    while (points.GetPoint(pId, param)) {
      Model::AngleInput in{param[0], param[1], param[2],
                           param[3], param[4], param[5],
                           param[6], param[7], param[8],
                           param[9], param[10], param[11],
                           param[12], static_cast<Model::Yuk>(param[13]),
                           Constants::vEW};
      Model::ParameterPoint p(in);
      if (Model::Valid(p) && uni(p) && bfb(p) && bphys(p) && stu(p)) {
        Model::CalcCouplings(p); // now we need the couplings
        if (edm(p)) {
          Model::RunHdecay(p); // for higgs we need the BR
          if (higgs(p)
#ifdef MicrOMEGAs_FOUND
              && dm(p)
#endif
          ) {
            Model::CalcCXNs(p); // we want the 13TeV cxns in the output
            out(p, pId);
          }
        }
      }
    }
    return 0;
  } //Check mode

  } // Switch mode

  // return 0;
} //Main function
