#include "ScannerS/Utilities.hpp"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/LU>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <gsl/gsl_poly.h>
#include <iterator>
#include <sstream>
#include <utility>
#define _USE_MATH_DEFINES
// IWYU pragma: no_include "src/Core/DenseBase.h"

namespace ScannerS::Utilities {

std::array<double, 6> MixMatAngles4d(const Eigen::Matrix4d &mixMat) {
  using std::acos;
  using std::asin;
  using std::cos;
  using std::pow;
  using std::sin;
  using std::sqrt;
  using std::tan;

  double a6 = asin(-mixMat(0, 3));
  double a2 = asin(mixMat(0, 2) / cos(a6));
  double a5 = asin(mixMat(2, 3) / -cos(a6));
  double a1 = asin(mixMat(0, 1) / (cos(a2) * cos(a6)));
  double a4 = asin(mixMat(1, 3) / (-cos(a5) * cos(a6)));
  double a3 = asin((mixMat(2, 0) * sin(a1) - mixMat(2, 1) * cos(a1)) / cos(a5));

  return {a1, a2, a3, a4, a5, a6};
}

std::array<double, 3> MixMatAngles3d(const Eigen::Matrix3d &mixMat) {
  return {std::atan(mixMat(0, 1) / mixMat(0, 0)), std::asin(mixMat(0, 2)),
          std::atan(mixMat(1, 2) / mixMat(2, 2))};
}

Eigen::Matrix3d MixMat3d(double a1, double a2, double a3) {
  return Eigen::Matrix3d{Eigen::AngleAxisd(-a3, Eigen::Vector3d::UnitX()) *
                         Eigen::AngleAxisd(a2, Eigen::Vector3d::UnitY()) *
                         Eigen::AngleAxisd(-a1, Eigen::Vector3d::UnitZ())};
}

Eigen::Matrix4d MixMat4d(double a1, double a2, double a3, double a4, double a5,
                         double a6) {
  using std::cos;
  using std::sin;
  const double c1 = cos(a1);
  const double s1 = sin(a1);
  const double c2 = cos(a2);
  const double s2 = sin(a2);
  const double c3 = cos(a3);
  const double s3 = sin(a3);
  const double c4 = cos(a4);
  const double s4 = sin(a4);
  const double c5 = cos(a5);
  const double s5 = sin(a5);
  const double c6 = cos(a6);
  const double s6 = sin(a6);
  Eigen::Matrix4d MixMat4d;
  MixMat4d << c1 * c2 * c6, c2 * c6 * s1, c6 * s2, -s6, // row1
      -(s1 * (c3 * c4 + s3 * s4 * s5)) +
          c1 * (-(s2 * (c4 * s3 - c3 * s4 * s5)) - c2 * c5 * s4 * s6),
      c1 * (c3 * c4 + s3 * s4 * s5) +
          s1 * (-(s2 * (c4 * s3 - c3 * s4 * s5)) - c2 * c5 * s4 * s6),
      c2 * (c4 * s3 - c3 * s4 * s5) - c5 * s2 * s4 * s6,
      -(c5 * c6 * s4), // row2
      c5 * s1 * s3 + c1 * (-(c3 * c5 * s2) - c2 * s5 * s6),
      -(c1 * c5 * s3) + s1 * (-(c3 * c5 * s2) - c2 * s5 * s6),
      c2 * c3 * c5 - s2 * s5 * s6, -(c6 * s5), // row3
      -(s1 * (c3 * s4 - c4 * s3 * s5)) +
          c1 * (-(s2 * (s3 * s4 + c3 * c4 * s5)) + c2 * c4 * c5 * s6),
      c1 * (c3 * s4 - c4 * s3 * s5) +
          s1 * (-(s2 * (s3 * s4 + c3 * c4 * s5)) + c2 * c4 * c5 * s6),
      c2 * (s3 * s4 + c3 * c4 * s5) + c4 * c5 * s2 * s6, c4 * c5 * c6; // row4

  return MixMat4d;
}

Eigen::Matrix3d OrderedMixMat3d(double a1, double a2, double a3,
                                const std::array<double, 3> &mHi) {
  auto R = MixMat3d(a1, a2, a3);
  R = TranspositionMatrixThatSorts(mHi) * R;
  Utilities::MixMatNormalForm3d(R);
  return R;
}

Eigen::Matrix4d OrderedMixMat4d(double a1, double a2, double a3, double a4,
                                double a5, double a6,
                                const std::array<double, 4> &mHi) {
  auto R = MixMat4d(a1, a2, a3, a4, a5, a6);
  R = Eigen::PermutationMatrix<4>{Eigen::Map<Eigen::Vector4i>{
                                      Utilities::IndexSort(mHi).data()}}
          .transpose() *
      R;
  Utilities::MixMatNormalForm4d(R);
  return R;
}

double AbsMax(const std::vector<double> &vals) {
  auto compareAbsoluteValue = [](double x, double y) -> bool {
    return std::abs(x) < std::abs(y);
  };
  return std::abs(
      *std::max_element(vals.begin(), vals.end(), compareAbsoluteValue));
}

void MixMatNormalForm3d(Eigen::Matrix3d &mixMat) {
  if (mixMat(0, 0) < 0)
    mixMat.row(0) *= -1;
  if (mixMat(2, 2) < 0)
    mixMat.row(2) *= -1;
  if (mixMat.determinant() < 0)
    mixMat.row(1) *= -1;
}

void MixMatNormalForm4d(Eigen::Matrix4d &mixMat) {
  using std::pow;
  if (mixMat(3, 3) < 0)
    mixMat.row(3) *= -1;
  if (mixMat(0, 0) < 0)
    mixMat.row(0) *= -1;
  if ((mixMat(2, 2) + (mixMat(0, 2) * mixMat(0, 3) * mixMat(2, 3)) /
                          (1 - pow(mixMat(0, 3), 2)) <
       0))
    mixMat.row(2) *= -1;
  if (mixMat.determinant() < 0)
    mixMat.row(1) *= -1;
}

std::vector<double> CubicRoots(double a, double b, double c) {
  double x0, x1, x2;

  size_t n = gsl_poly_solve_cubic(a, b, c, &x0, &x1, &x2);
  if (n == 1) {
    return {x1};
  }
  return {x0, x1, x2};
}

std::vector<double> ParseToDoubles(const std::string &str) {
  std::istringstream iss(str);
  std::vector<double> result;
  std::copy(std::istream_iterator<double>(iss), std::istream_iterator<double>(),
            std::back_inserter(result));
  return result;
}

std::vector<std::string> SplitString(const std::string &s, char delimiter) {
  std::vector<std::string> tokens;
  std::istringstream tokenStream{s};
  std::string token;
  while (std::getline(tokenStream, token, delimiter))
    tokens.push_back(token);
  return tokens;
}

DataMap::Map ZipToMap(std::vector<DataMap::key_type> &&keys,
                      const std::vector<DataMap::mapped_type> &values) {
  assert(keys.size() == values.size());

  auto result = DataMap::Map{};
  for (size_t i = 0; i != keys.size(); ++i)
    result.emplace(std::move(keys[i]), values[i]);
  return result;
}

const Eigen::IOFormat TSVPrinter::matrixFormat{
    Eigen::StreamPrecision, Eigen::DontAlignCols,
    Utilities::TSVPrinter::separator, Utilities::TSVPrinter::separator};

} // namespace ScannerS::Utilities